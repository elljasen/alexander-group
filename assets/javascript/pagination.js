(function($) {

  function find_page_number( element ) {
    element.find('span').remove();
    return parseInt( element.html() );
  }

  $(document).on( 'click', 'a.page-numbers', function( event ) {

    event.preventDefault();
    //
    // $('a.page-numbers.active').removeClass('active')
    // $(this).addClass('active')


    var page = find_page_number( $(this).clone() );

    TweenLite.to($('.AjaxLoader'), 0.5, {
      opacity: 1,
      display: 'flex',
      ease: Power2.easeOut,
      zIndex: 1
    })


    $.ajax({
      type: 'POST',
      url: ajaxpagination.ajaxurl,
      data: {
        action: 'ajax_pagination',
        page: page
      },
      success: function( html ) {
        TweenLite.to($('.AjaxLoader'), 0.5, {
          opacity: 0,
          display: 'none',
          ease: Power2.easeOut,
          zIndex: -1
        })
        $('.articleList').empty()
        $('.navigation.pagination').remove()
        $('.articleList').append(html)
      }
    })


  })

})(jQuery);