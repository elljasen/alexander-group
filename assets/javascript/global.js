// theme javascript :

// Hamburger Menu :

const mobileMenuButton = document.querySelector('.Hamburger');
const mobileMenu = document.querySelector('.Navigation.Mobile');
const closeText = document.querySelector('.Navigation.Mobile .close');

mobileMenuButton.addEventListener('click', function () {
  mobileMenu.classList.toggle('MobileOpen')
}, false)

closeText.addEventListener('click', function () {
  mobileMenu.classList.toggle('MobileOpen')
}, false)

// AJAX Pop Up

const ajaxButton = document.querySelectorAll('.AjaxButton')
const ajaxLayout = document.querySelector('.AjaxContainer')
const ajaxLoader = document.querySelector('.AjaxLoader');
const ajaxLoaderImage = document.querySelector('.AjaxLoader img')
const AjaxCloseButton = document.querySelector('.AjaxCloseButton')
const body = document.getElementsByTagName('body')[0]

// leadership :

const leadership = document.querySelectorAll('.TeamProfiles')

// services

const services = document.querySelectorAll('.services-list')

if (typeof (ajaxLayout) != 'undefined' && ajaxLayout != null) {
  AjaxCloseButton.addEventListener('click', function () {
    TweenLite.to(ajaxLoader, 0.5, {
      opacity: 0,
      ease: Power2.easeIn,
      zIndex: -1,
      onStart: function () {
        TweenLite.to(ajaxLayout, .2, {
          opacity: 0,
          ease: Power2.easeIn
        })
        body.classList.remove('noScroll')
      }
    })
  })

  // services page :

  if (typeof services[0] !== 'undefined') {

    // querySelectorAll returns a node list - need to loop through the list so the below applies to all AJAX Buttons

    for (var i = 0; i < ajaxButton.length; i++) {

      ajaxButton[i].addEventListener('click', function () {
        const ServiceID = this.getAttribute('data-id')
        getServiceDescription(ServiceID)
      })
    }

    function getServiceDescription(ServiceID) {

      axios.interceptors.request.use(function (config) {
        TweenLite.to(ajaxLoader, 0.5, {
          opacity: 1,
          ease: Power2.easeOut,
          zIndex: 30,
          onStart: function () {
            body.classList.add('noScroll')
          }
        })
        TweenLite.to(ajaxLoaderImage, 0.5, {
          opacity: 1,
          ease: Power2.easeOut,
        })
        // Do something before request is sent
        return config;
      }, function (error) {
        // Do something with request error
        return Promise.reject(error);
      })

      axios.interceptors.response.use(function (response) {
        // Do something with response data
        console.log(response)
        return response;
      }, function (error) {
        // Do something with response error
        return Promise.reject(error);
      });

      axios.get(pageParams.root + '/wp-json/wp/v2/tag_services/' + ServiceID)
        .then(function (response) {
          TweenLite.to(ajaxLoaderImage, 0.5, {
            opacity: 0,
            ease: Power2.easeIn,
          })
          TweenLite.to(ajaxLayout, .2, {
            opacity: 1,
            ease: Power2.easeOut
          })

          const Description = response.data.content.rendered
          const Title = response.data.title.rendered
          document.querySelector('.servicesTitle span')
            .innerHTML = ''
          document.querySelector('.servicesTitle span')
            .innerHTML = Title

          document.querySelector('.serviceDescription .inner')
            .innerHTML = ''
          document.querySelector('.serviceDescription .inner')
            .innerHTML = Description
        })
        .catch(function (error) {
          console.log(error);

        })


    }

  }

  // leadership page :

  if (typeof leadership[0] !== 'undefined') {

    // querySelectorAll returns a node list - need to loop through the list so the below applies to all AJAX Buttons

    for (var i = 0; i < ajaxButton.length; i++) {

      ajaxButton[i].addEventListener('click', function () {

        const ProfileID = this.getAttribute('data-id')
        getProfileData(ProfileID)

      })
    }

    function getProfileData(ProfileID) {

      axios.interceptors.request.use(function (config) {
        TweenLite.to(ajaxLoader, 0.5, {
          opacity: 1,
          ease: Power2.easeOut,
          zIndex: 30,
          onStart: function () {
            body.classList.add('noScroll')
          }
        })
        TweenLite.to(ajaxLoaderImage, 0.5, {
          opacity: 1,
          ease: Power2.easeOut,
        })
        // Do something before request is sent
        return config;
      }, function (error) {
        // Do something with request error
        return Promise.reject(error);
      })

      axios.interceptors.response.use(function (response) {
        // Do something with response data
        return response;
      }, function (error) {
        // Do something with response error
        return Promise.reject(error);
      });

      axios.get(pageParams.root + '/wp-json/wp/v2/team_profiles/' + ProfileID)
        .then(function (response) {
          TweenLite.to(ajaxLoaderImage, 0.5, {
            opacity: 0,
            ease: Power2.easeIn,
          })
          TweenLite.to(ajaxLayout, .2, {
            opacity: 1,
            ease: Power2.easeOut
          })

          const Title = response.data.title.rendered
          document.querySelector('.TeamProfileFull .Name h1')
            .innerHTML = ''
          document.querySelector('.TeamProfileFull .Name h1')
            .innerHTML = Title

          const Image = response.data.acf.image.url
          document.querySelector('.TeamProfileFull .Image')
            .style.backgroundImage = ''
          document.querySelector('.TeamProfileFull .Image')
            .style.backgroundImage = 'url(' + Image + ')'

          const Label = response.data.acf.label
          document.querySelector('.TeamProfileFull .Content h1')
            .innerHTML = ''
          document.querySelector('.TeamProfileFull .Content h1')
            .innerHTML = Label + ' &ndash;'

          const Description = response.data.acf.content
          document.querySelector('.TeamProfileFull .Content .Description')
            .innerHTML = ''
          document.querySelector('.TeamProfileFull .Content .Description')
            .innerHTML = Description

          const ExternalLink = response.data.acf.external_link
          document.querySelector('.TeamProfileFull a.btn')
            .href = ''
          document.querySelector('.TeamProfileFull a.btn')
            .href = ExternalLink

          const ExperienceLabel = response.data.acf.experience_label
          document.querySelector('.TeamProfileFull .ExperienceLabel h1')
            .innerHTML = ''
          document.querySelector('.TeamProfileFull .ExperienceLabel h1')
            .innerHTML = ExperienceLabel

          const ExperienceBullets = response.data.acf.experience_bullets
          const ExperienceList = document.querySelector('.TeamProfileFull .ExperienceList')
          ExperienceList.innerHTML = ''

          for (var i = 0; i < ExperienceBullets.length; i++) {
            const ExperienceLabel = ExperienceBullets[i].label
            const ExperienceContent = ExperienceBullets[i].content
            const ListElement = 'li'
            const ListElementSpan = 'span'
            const ExperienceBullet = document.createElement(ListElement)
            ExperienceBullet.innerHTML = '<h1>' + ExperienceLabel + '</h1>' + ExperienceContent
            ExperienceList.appendChild(ExperienceBullet)
          }

        })
        .catch(function (error) {
          console.log(error);
        });

    }
  }
}

// locations page :

const LocationPicker = document.querySelector('.FlexContainer.Locations')
const LocationButton = document.querySelectorAll('.LocationContainer')
const Locations = document.querySelectorAll('.LocationBox')
const Maps = document.querySelectorAll('.acf-map')

if (typeof (LocationPicker) != 'undefined' && LocationPicker != null) {
  // querySelectorAll returns a node list - need to loop through the list so the below applies to all .LocationButtons

  for (var i = 0; i < LocationButton.length; i++) {

    LocationButton[i].addEventListener('click', function () {

      // remove active class from selected button

      const currentButton = document.querySelector('.LocationContainer.activeLocation')
      currentButton.classList.remove('activeLocation')

      // remove active class from selected location

      const currentLocation = document.querySelector('.FlexContainer.activeLocation')
      currentLocation.classList.remove('activeLocation')

      const LocationID = this.getAttribute('data-location-id')

      // add active class to clicked button

      this.classList.add('activeLocation')

      // add active to current location

      for (var i = 0; i < Locations.length; i++) {
        if (Locations[i].getAttribute('data-location-id') == LocationID) {
          Locations[i].classList.add('activeLocation')

          // trigger map resize so it displays correctly when a new location is selected

          google.maps.event.trigger(Maps[i], 'resize')
        }
      }

    })
  }
}

// home page scroll down

const scrollButton = document.querySelector('.PageBanner.Home .ScrollDown')

if (typeof (scrollButton) != 'undefined' && scrollButton != null) {
  scrollButton.addEventListener('click', function() {
    TweenLite.to(window, 2, {
      scrollTo: 600,
      ease: Power2.easeOut
    })
  })
}

// blog -- news selection
// const blogContent = document.querySelector('.blogContent')
//
//
// if (typeof (blogContent) != 'undefined' && blogContent != null) {
//   const blogButton = document.querySelectorAll('.blogContent .btn')
//
//   for( var i = 0; i < blogButton.length; i++) {
//     blogButton[i].addEventListener('click', function() {
//       const blogType = this.getAttribute('data-name')
//       const currentBlogType = document.querySelector('.PostContainerWrap.active')
//       currentBlogType.classList.remove('active')
//       document.querySelector('.blogContent .btn.active').classList.remove('active')
//
//       document.querySelector('.PostContainerWrap.'+blogType).classList.add('active')
//       document.querySelector('[data-name='+blogType+']').classList.add('active')
//     })
//   }
// }