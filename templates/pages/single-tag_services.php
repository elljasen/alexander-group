<?php
/**
 * Created by PhpStorm.
 * User: jasenpeterson
 * Date: 8/8/17
 * Time: 2:11 PM
 * Description: Single Service Query
 */

use PostQuery\PostQuery;

// reterive post(s) :

$GetPost = new PostQuery();

$GetPost->getPost('tag_services', 1);
$smarty->assign('RecentPost', $GetPost->CollectedPost);

$post = get_post($pageID);

$smarty->assign('ServiceName', $post->post_title);
$smarty->assign('ServiceContent', wpautop($post->post_content) );

// if template exists :

if ($smarty->templateExists(THEME_DIR . '/smarty_templates/pages/single-tag_services.tpl')) :

    // display template :

    $smarty->display(THEME_DIR . '/smarty_templates/pages/single-tag_services.tpl');

endif;