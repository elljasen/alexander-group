<?php

include(locate_template('/templates/global/vars.php'));

// page banner :

$smarty->assign('AboutPageBanner', get_field('page_banner', $PageID));

// about page intro - flexible content

$smarty->assign('AboutPageIntro', get_field('intro', $PageID));

// culture video :

$smarty->assign('Video', get_field('video', $PageID));

// our culture :

$smarty->assign('OurCulture', get_field('our_culture', $PageID));

// if template exists :

if ($smarty->templateExists(THEME_DIR . '/smarty_templates/pages/about.tpl')) :

    // display template :

    $smarty->display(THEME_DIR . '/smarty_templates/pages/about.tpl');

endif;
