<?php
include(locate_template('/templates/global/vars.php'));

// home page :

use ContactPage\ContactPage;

$ContactPage = new ContactPage(34, 1, 'page');
$ContactPage->displayPage();

// smarty :

// page banner :

$smarty->assign('PageBanner', $ContactPage->PageBanner);

// contact :

$smarty->assign('Content', $ContactPage->Content);

$smarty->assign('Title', $ContactPage->Title);

$ContactForm = do_shortcode('[contact-form-7 id="481" title="Primary Contact"]');

$smarty->assign('ContactForm', $ContactForm);

$smarty->assign('Map', $ContactPage->Map);

// if template exists :

if ($smarty->templateExists(THEME_DIR . '/smarty_templates/pages/contact.tpl')) :

    // display template :

    $smarty->display(THEME_DIR . '/smarty_templates/pages/contact.tpl');

endif;

?>
