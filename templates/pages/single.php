<?php

use PostQuery\PostQuery;

// reterive post(s) :

$GetPost = new PostQuery();

$GetPost->getPost('post', 9); // post type, post per page
$smarty->assign('RecentPost', $GetPost->CollectedPost);

$post = get_post($pageID);

$smarty->assign('SinglePost', $post);
$smarty->assign('SinglePostContent', wpautop($post->post_content));
$smarty->assign('SinglePostImage', get_the_post_thumbnail_url($pageID));
$smarty->assign('SinglePostDate', get_the_date('M. j, Y', $pageID));
$smarty->assign('SinglePostAuthor', get_the_author_meta('display_name', $postAuthor));

// if template exists :

if ($smarty->templateExists(THEME_DIR . '/smarty_templates/pages/single.tpl')) :

    // display template :

    $smarty->display(THEME_DIR . '/smarty_templates/pages/single.tpl');

endif;
