<?php

include(locate_template('/templates/global/vars.php'));

// page banner :

$smarty->assign('ServicesPageBanner', get_field('page_banner', $PageID));

// content :

$smarty->assign('ServicesContent', get_field('content', $PageID));

// services :
$smarty->assign('IndustryServices', get_field('industry_services', $PageID));
$smarty->assign('FunctionServices', get_field('function_services', $PageID));

// if template exists :

if ($smarty->templateExists(THEME_DIR . '/smarty_templates/pages/services.tpl')) :

    // display template :

    $smarty->display(THEME_DIR . '/smarty_templates/pages/services.tpl');

endif;
