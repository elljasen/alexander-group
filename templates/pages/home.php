<?php
include(locate_template('/templates/global/vars.php'));

use PostQuery\PostQuery;

use PagePortal\PagePortal;

// page portals :
$pagePortals = array(19,23,27);
$pagePortal = new PagePortal($pagePortals);
$pagePortal->displayPagePortal();

// reterive post(s) :

$GetPost = new PostQuery();

// page portals array:
$smarty->assign('PagePortalArray', $pagePortal->PagePortalArray);

// --- about :

$smarty->assign('AboutPagePortal', $pagePortal->PagePortalArray['about'] );
$smarty->assign('CompanyMotto', $pagePortal->CompanyMotto );

// --- about - news :
$GetPost->getPost('post',3,null); // post type, post per page, acf field
$smarty->assign('NewPostObject', $GetPost->CollectedPost );

// --- expertise :

$smarty->assign('ExpertisePagePortal', $pagePortal->PagePortalArray['expertise'] );

// --- locations :

$smarty->assign('LocationsPagePortal', $pagePortal->PagePortalArray['locations'] );
$smarty->assign('Locations', $pagePortal->Locations );

// news list:

// -- news page :

$newsPostID = 29;
$newsPost = get_post($newsPostID);
$newsPostSlug = $newsPost->post_name;
$smarty->assign('NewsPostSlug', $newsPostSlug);

// publications :
$GetPost->getPost('publication',3,'publications'); // post type, post per page, acf field
$smarty->assign('Publications', $GetPost->CollectedPost );

// page banner :

$smarty->assign('PageBanner', get_field('page_banner', $PageID));


// if template exists :

if ($smarty->templateExists(THEME_DIR . '/smarty_templates/pages/home.tpl')) :

    // display template :

    $smarty->display(THEME_DIR . '/smarty_templates/pages/home.tpl');

endif;