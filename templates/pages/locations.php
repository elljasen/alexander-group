<?php

include(locate_template('/templates/global/vars.php'));

// page banner :

$smarty->assign('LocationsBanner', get_field('page_banner', $PageID));

// --- locations :

$smarty->assign('Locations', get_field('locations', $PageID));

// --- locations title :

$smarty->assign('LocationsTitle', get_field('locations_title', $PageID));

// --- locations content :

$smarty->assign('LocationsContent', get_field('locations_content', $PageID));

// --- contact form :

$smarty->assign('LocationsForm', do_shortcode('[contact-form-7 id="728" title="Locations Form"]'));

// if template exists :

if ($smarty->templateExists(THEME_DIR . '/smarty_templates/pages/locations.tpl')) :

    // display template :

    $smarty->display(THEME_DIR . '/smarty_templates/pages/locations.tpl');

endif;
