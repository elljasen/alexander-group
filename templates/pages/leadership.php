<?php

include(locate_template('/templates/global/vars.php'));

// page banner :

$smarty->assign('TeamPageBanner', get_field('page_banner', $PageID));

// team profiles :

$smarty->assign('TeamProfiles', get_field('team', $PageID));

// if template exists :

if ($smarty->templateExists(THEME_DIR . '/smarty_templates/pages/leadership.tpl')) :

    // display template :

    $smarty->display(THEME_DIR . '/smarty_templates/pages/leadership.tpl');

endif;
