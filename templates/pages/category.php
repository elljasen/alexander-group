<?php

include(locate_template('/templates/global/vars.php'));

use PostQuery\PostQuery;
use TwitterFeed\TwitterFeed;

// twitter feed :

$TwitterFeed = new TwitterFeed();

$TwitterFeed->DisplayFeed();
$smarty->assign('TwitterFeed', $TwitterFeed->content);


// reterive post(s) :

$GetPost = new PostQuery();

$postArray = array('post','tag_news');

$GetPost->getPost($postArray, 8); // post type, post per page, acf field
$smarty->assign('BlogPostObject', $GetPost->CollectedPost);

// if template exists :

if ($smarty->templateExists(THEME_DIR . '/smarty_templates/pages/category.tpl')) :

    // display template :

    $smarty->display(THEME_DIR . '/smarty_templates/pages/category.tpl');

endif;
