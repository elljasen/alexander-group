<?php

include(locate_template('/templates/global/vars.php'));

// page banner :

$smarty->assign('ExpertiseBanner', get_field('page_banner', $PageID));

// banner content :

$smarty->assign('ExpertiseBannerContent', get_field('banner_content', $PageID));

// expertise page intro - flexible content

$smarty->assign('ExpertiseIntro', get_field('intro', $PageID));

// expertise page break

$smarty->assign('ExpertisePageBreak', get_field('page_break', $PageID));

// expertise page break image

$smarty->assign('ExpertisePageBreakImage', get_field('page_break_image', $PageID));


// if template exists :

if ($smarty->templateExists(THEME_DIR . '/smarty_templates/pages/expertise.tpl')) :

    // display template :

    $smarty->display(THEME_DIR . '/smarty_templates/pages/expertise.tpl');

endif;
