<?php
/**
 * Created by PhpStorm.
 * User: jasenpeterson
 * Date: 8/8/17
 * Time: 2:11 PM
 * Description: Single Team Profiles Query
 */

use PostQuery\PostQuery;

// reterive post(s) :

$GetPost = new PostQuery();

$GetPost->getPost('team_profiles', 1);
$smarty->assign('RecentPost', $GetPost->CollectedPost);

$post = get_post($pageID);

$smarty->assign('ProfileName', $post->post_title);
$smarty->assign('ProfileImage', get_field('image', $pageID));
$smarty->assign('ProfileLabel', get_field('label', $pageID));
$smarty->assign('ProfileContent', get_field('content', $pageID));
$smarty->assign('ProfileLink', get_field('external_link', $pageID));
$smarty->assign('ExperienceLabel', get_field('experience_label', $pageID));
$smarty->assign('ExperienceBullets', get_field('experience_bullets', $pageID));

// if template exists :

if ($smarty->templateExists(THEME_DIR . '/smarty_templates/pages/single-team_profiles.tpl')) :

    // display template :

    $smarty->display(THEME_DIR . '/smarty_templates/pages/single-team_profiles.tpl');

endif;