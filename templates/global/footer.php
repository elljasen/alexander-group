<?php

include(locate_template('/templates/global/vars.php'));
include(locate_template('/templates/global/navigation.php'));

// smarty :

// if template exists :

if ($smarty->templateExists(THEME_DIR . '/smarty_templates/global/footer.tpl')) :

    // display template :

    $smarty->display(THEME_DIR . '/smarty_templates/global/footer.tpl');

endif;
