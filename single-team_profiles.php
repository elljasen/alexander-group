<?php
/**
 * Created by PhpStorm.
 * User: jasenpeterson
 * Date: 8/8/17
 * Time: 2:15 PM
 * Description: Team Profiles Template Page
 */

get_header();

get_template_part('templates/global/header');

get_template_part('loops/single-team_profiles', 'loop');

get_template_part('templates/global/footer');

get_footer();