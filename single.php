<?php

get_header();

get_template_part('templates/global/header');

get_template_part('loops/single', 'loop');

get_template_part('templates/global/footer');

get_footer();
