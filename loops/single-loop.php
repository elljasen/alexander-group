<section class="ContentWrap SinglePost">
    <?php

      include(locate_template('/templates/global/vars.php'));

      // get the queried object and sanitize it :

      $current_page = sanitize_post($GLOBALS['wp_the_query']->get_queried_object());

      // get the page slug :

      $postAuthor = $current_page->post_author;

      $pageSlug = $current_page->post_name;

      // get page ID :

      $pageID = $current_page->ID;

      include(locate_template('/templates/pages/single.php'));

    ?>
</section>
