<?php

include(locate_template('/templates/global/vars.php'));



$smarty->assign('postTitle', get_the_title());
$smarty->assign('postImage', wp_get_attachment_image_src( get_post_thumbnail_id( $pageID )));
$smarty->assign('postDate', get_the_date('M. j, Y'));
$smarty->assign('postAuthor', get_the_author());
$smarty->assign( 'postURL', get_the_permalink());
// if template exists :

if ($smarty->templateExists(THEME_DIR . '/smarty_templates/post/ajax-post.tpl')) :

    // display template :

    $smarty->display(THEME_DIR . '/smarty_templates/post/ajax-post.tpl');

endif;