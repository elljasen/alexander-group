<?php
/* Smarty version 3.1.30, created on 2017-07-12 19:41:48
  from "/Users/jasenpeterson/Sites/tag/wp-content/themes/TAG/smarty_templates/pages/services.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_59667b7c5e05f7_79488531',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'bb481c59bfb8924a82819bb079a471677628c841' => 
    array (
      0 => '/Users/jasenpeterson/Sites/tag/wp-content/themes/TAG/smarty_templates/pages/services.tpl',
      1 => 1499888502,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:../global/pageBanner.tpl' => 1,
    'file:../global/servicesDescription.tpl' => 1,
  ),
),false)) {
function content_59667b7c5e05f7_79488531 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_subTemplateRender("file:../global/pageBanner.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('Banner'=>$_smarty_tpl->tpl_vars['ServicesPageBanner']->value,'BannerClass'=>'Services Interior'), 0, false);
?>


<section class="Services Horizontal FlexContainer">

    <!-- ROW -->
    <section class="row RowOne">

      <div class="column LightBlueBackgroundBlock"></div>

      <section class="column">
        <section class="box">
          <h1><?php echo $_smarty_tpl->tpl_vars['ServicesContent']->value[0]['row'][0]['label'];?>
 &ndash;</h1>
          <?php echo $_smarty_tpl->tpl_vars['ServicesContent']->value[0]['row'][0]['content'];?>

        </section>
      </section>

      <div class="column DarkGrayBackground"></div>

    </section>
    <!-- /ROW -->

    <!-- ROW -->
    <section class="row RowTwo">

      <section class="column">
        <section class="BackgroundImage" style="background: url(<?php echo $_smarty_tpl->tpl_vars['ServicesContent']->value[0]['row'][1]['image']['url'];?>
)"></section>
        <section class="box">
          <?php echo $_smarty_tpl->tpl_vars['ServicesContent']->value[0]['row'][2]['content'];?>

        </section>
      </section>

      <section class="column">
        <section class="box service-content">
          <?php echo $_smarty_tpl->tpl_vars['ServicesContent']->value[0]['row'][1]['content'];?>

        </section>
        <section class="services-list">
          <section class="box">
            <h1>Industry &ndash;</h1>
            <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['IndustryServices']->value, 'service');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['service']->value) {
?>
            <a href="javascript:void(0)" class="btn AjaxButton" data-id="<?php echo $_smarty_tpl->tpl_vars['service']->value['description'];?>
">
              <span><?php echo $_smarty_tpl->tpl_vars['service']->value['name'];?>
</span>
              <div class="line"></div>
            </a>
            <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl);
?>

            <h1>Function &ndash;</h1>
            <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['FunctionServices']->value, 'service');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['service']->value) {
?>
            <a href="javascript:void(0)" class="btn AjaxButton" data-id="<?php echo $_smarty_tpl->tpl_vars['service']->value['description'];?>
">
              <span><?php echo $_smarty_tpl->tpl_vars['service']->value['name'];?>
</span>
              <div class="line"></div>
            </a>
            <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl);
?>

          </section>
          <section class="BackgroundImage" style="background: url(<?php echo $_smarty_tpl->tpl_vars['ServicesContent']->value[0]['row'][2]['image']['url'];?>
)"></section>
        </section>
      </section>

    </section>
    <!-- /ROW -->

</section>



<?php $_smarty_tpl->_subTemplateRender("file:../global/servicesDescription.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

<?php }
}
