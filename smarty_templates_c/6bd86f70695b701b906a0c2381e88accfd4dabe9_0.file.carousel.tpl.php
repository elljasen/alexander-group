<?php
/* Smarty version 3.1.30, created on 2017-06-05 14:52:58
  from "/Users/jasenpeterson/Sites/haven/wp-content/themes/haven/smarty_templates/global/carousel.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_5935704a700896_24528280',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '6bd86f70695b701b906a0c2381e88accfd4dabe9' => 
    array (
      0 => '/Users/jasenpeterson/Sites/haven/wp-content/themes/haven/smarty_templates/global/carousel.tpl',
      1 => 1496674375,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5935704a700896_24528280 (Smarty_Internal_Template $_smarty_tpl) {
?>
<section class="Carousel">
    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 827 709" class="Slurve CarouselSlurve">
        <path d="M-1 51.8L827 0v709.1H-1V51.8z"/>
    </svg>
    <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['Carousel']->value, 'CarouselItem');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['CarouselItem']->value) {
?>
        <section class="CarouselCell">
            <section class="container">
                <section class="grid">
                    <section class="row row-align-middle row-align-center">
                        <div class="gr-6 gr-12@xs gr-12@sm CarouselImage">
                            <section class="Inner">
                                <img src="<?php echo $_smarty_tpl->tpl_vars['CarouselItem']->value['image']['url'];?>
" alt="<?php echo $_smarty_tpl->tpl_vars['CarouselItem']->value['image']['alt'];?>
"/>
                            </section>
                        </div>
                        <!--/CaruselImage-->
                        <div class="gr-6 gr-12@xs gr-12@sm CarouselContent">
                            <section class="box">
                              <svg class="Diamond TwoDiamonds" width="32px" height="32px" viewBox="0 0 40 32" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                                  <polygon points="16,2 30,16 16,30 2,16"></polygon>
                                  <polygon points="16,2 30,16 16,30 2,16"></polygon>
                              </svg>
                              <h1><?php echo $_smarty_tpl->tpl_vars['CarouselItem']->value['title'];?>
</h1>
                              <?php echo $_smarty_tpl->tpl_vars['CarouselItem']->value['content'];?>

                              <a href="<?php echo $_smarty_tpl->tpl_vars['CarouselItem']->value['button_link'];?>
" class="btn">
                                  <svg><rect x="0" y="0" fill="none" width="100%" height="100%"></rect></svg>
                                  Learn More
                              </a>
                            </section>
                        </div>
                        <!--/CarouselContent-->
                    </section>
                </section>
            </section>
        </section>
    <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl);
?>

</section>
<?php }
}
