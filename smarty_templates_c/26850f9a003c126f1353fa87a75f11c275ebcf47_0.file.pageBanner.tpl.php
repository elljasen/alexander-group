<?php
/* Smarty version 3.1.30, created on 2017-06-14 20:04:41
  from "/Users/jasenpeterson/Sites/tag/wp-content/themes/TAG/smarty_templates/global/pageBanner.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_594196d9bad304_29512321',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '26850f9a003c126f1353fa87a75f11c275ebcf47' => 
    array (
      0 => '/Users/jasenpeterson/Sites/tag/wp-content/themes/TAG/smarty_templates/global/pageBanner.tpl',
      1 => 1497470679,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_594196d9bad304_29512321 (Smarty_Internal_Template $_smarty_tpl) {
if (!empty($_smarty_tpl->tpl_vars['Banner']->value)) {?>
  <section class="PageBanner <?php echo $_smarty_tpl->tpl_vars['BannerClass']->value;?>
" style="background: url(<?php echo $_smarty_tpl->tpl_vars['Banner']->value[0]['image']['url'];?>
)">
      <section class="PageBannerContent">

        <?php if (!empty($_smarty_tpl->tpl_vars['Banner']->value[0]['title'])) {?>
           <h1><?php echo $_smarty_tpl->tpl_vars['Banner']->value[0]['title'];?>
</h1>
          <?php } else { ?>
          <h1 class="Flex">
            <span><?php echo $_smarty_tpl->tpl_vars['pageTitle']->value;?>
 &ndash;</span>
            <svg xmlns="http://www.w3.org/2000/svg" class="PlayButtonIcon" viewBox="0 0 410 411">
              <path d="M284.6 198.1l-108.1-73.9c-3.4-2.5-12.3-2.5-13 6.9v147.8c.8 9.4 9.9 9.5 13 6.9l108.1-73.9c2.8-1.7 6.6-8.6 0-13.8zm-104.4 65.1V146.8L265 205l-84.8 58.2zm0 0" class="st0"/>
              <path d="M206 9C97.9 9 10 96.9 10 205s87.9 196 196 196 196-87.9 196-196S314.1 9 206 9zm0 375.3c-98.9 0-179.3-80.4-179.3-179.3S107.1 25.7 206 25.7 385.3 106.1 385.3 205 304.9 384.3 206 384.3zm0 0" class="st0"/>
            </svg>
          </h1>
        <?php }?>

        <section class="ScrollDown">
          <span>Learn More</span>
          <svg xmlns="http://www.w3.org/2000/svg" viewBox="926.945 947.945 60.109 60.109">
            <g transform="translate(811.5 954.5)">
                <circle cx="28.055" cy="28.055" r="28.055" transform="translate(117.445 -4.555)"></circle>
                <path d="M129.52 21.014l16.336 7.813 16.336-7.813"></path>
            </g>
          </svg>
        </section>
      </section>
      <section class="tint"></section>
  </section>
<?php }
}
}
