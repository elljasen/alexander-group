<?php
/* Smarty version 3.1.30, created on 2017-06-15 15:06:11
  from "/Users/jasenpeterson/Sites/tag/wp-content/themes/TAG/smarty_templates/portals/expertisePortal.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_5942a263bbe0b7_78444638',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'd740f6e46ab51246b4e600b9b737c1062868b467' => 
    array (
      0 => '/Users/jasenpeterson/Sites/tag/wp-content/themes/TAG/smarty_templates/portals/expertisePortal.tpl',
      1 => 1497539169,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5942a263bbe0b7_78444638 (Smarty_Internal_Template $_smarty_tpl) {
if (!empty($_smarty_tpl->tpl_vars['ExpertisePortal']->value)) {?>

<section class="FlexContainer ExpertisePortalFlexContainer PagePortal">

  <section class="row">
    <section class="BackgroundImage column" style="background: url('<?php echo $_smarty_tpl->tpl_vars['ExpertisePortal']->value['pagePortal'][0]['image']['url'];?>
') no-repeat;"></section>
    <section class="Content column">
      <section class="box PortalContent">
        <h1><?php echo $_smarty_tpl->tpl_vars['ExpertisePortal']->value['pagePortal'][0]['title'];?>
 &ndash;</h1>
        <?php echo $_smarty_tpl->tpl_vars['ExpertisePortal']->value['pagePortal'][0]['content'];?>

        <a class="btn" href="<?php echo $_smarty_tpl->tpl_vars['homeURL']->value;?>
/<?php echo $_smarty_tpl->tpl_vars['ExpertisePortal']->value['slug'];?>
">
          <span>Learn More</span>
          <div class="line"></div>
        </a>
      </section>
    </section>
  </section>

</section>

<?php }
}
}
