<?php
/* Smarty version 3.1.30, created on 2017-06-06 16:25:29
  from "/Users/jasenpeterson/Sites/tag/wp-content/themes/TAG/smarty_templates/global/carousel.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_5936d7796cdc61_22733602',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '6cb7397bbbd1a3e51b20ae9527eb7e4f543bfe4f' => 
    array (
      0 => '/Users/jasenpeterson/Sites/tag/wp-content/themes/TAG/smarty_templates/global/carousel.tpl',
      1 => 1496766326,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5936d7796cdc61_22733602 (Smarty_Internal_Template $_smarty_tpl) {
?>
<section class="Carousel">
    <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['Carousel']->value, 'CarouselItem');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['CarouselItem']->value) {
?>
        <section class="CarouselCell">
            <section class="container">
                <section class="grid">
                    <section class="row row-align-middle row-align-center">
                        <div class="gr-6 gr-12@xs gr-12@sm CarouselImage">
                            <section class="Inner">
                                <img src="<?php echo $_smarty_tpl->tpl_vars['CarouselItem']->value['image']['url'];?>
" alt="<?php echo $_smarty_tpl->tpl_vars['CarouselItem']->value['image']['alt'];?>
"/>
                            </section>
                        </div>
                        <!--/CaruselImage-->
                        <div class="gr-6 gr-12@xs gr-12@sm CarouselContent">
                            <section class="box">
                              <svg class="Diamond TwoDiamonds" width="32px" height="32px" viewBox="0 0 40 32" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                                  <polygon points="16,2 30,16 16,30 2,16"></polygon>
                                  <polygon points="16,2 30,16 16,30 2,16"></polygon>
                              </svg>
                              <h1><?php echo $_smarty_tpl->tpl_vars['CarouselItem']->value['title'];?>
</h1>
                              <?php echo $_smarty_tpl->tpl_vars['CarouselItem']->value['content'];?>

                              <a href="<?php echo $_smarty_tpl->tpl_vars['CarouselItem']->value['button_link'];?>
" class="btn">
                                  <svg><rect x="0" y="0" fill="none" width="100%" height="100%"></rect></svg>
                                  Learn More
                              </a>
                            </section>
                        </div>
                        <!--/CarouselContent-->
                    </section>
                </section>
            </section>
        </section>
    <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl);
?>

</section>
<?php }
}
