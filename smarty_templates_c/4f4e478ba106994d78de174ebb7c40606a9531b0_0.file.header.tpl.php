<?php
/* Smarty version 3.1.30, created on 2017-06-05 17:01:19
  from "/Users/jasenpeterson/Sites/haven/wp-content/themes/haven/smarty_templates/global/header.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_59358e5f629ec2_20290516',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '4f4e478ba106994d78de174ebb7c40606a9531b0' => 
    array (
      0 => '/Users/jasenpeterson/Sites/haven/wp-content/themes/haven/smarty_templates/global/header.tpl',
      1 => 1496682076,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_59358e5f629ec2_20290516 (Smarty_Internal_Template $_smarty_tpl) {
?>
<header>
    <section class="container SecondaryHeader">
        <div class="BusinessPhone">
            <?php echo $_smarty_tpl->tpl_vars['BusinessPhone']->value;?>

        </div>
        <section class="Navigation Secondary">
            <?php echo $_smarty_tpl->smarty->registered_objects['nav'][0]->displaySecondaryMenu(array(),$_smarty_tpl);?>

        </section>
    </section>
    <section class="box">
        <section class="container PrimaryHeader">
            <div class="SiteLogo">
                <a href="<?php echo $_smarty_tpl->tpl_vars['homeURL']->value;?>
"><?php echo $_smarty_tpl->tpl_vars['SiteLogo']->value;?>
</a>
            </div>
            <!--/logo-->
            <div class="Navigation Primary">
                <?php echo $_smarty_tpl->smarty->registered_objects['nav'][0]->displayPrimaryMenu(array(),$_smarty_tpl);?>

            </div>
        </section>
    </section>
</header>
<section class="parallax"></section>
<svg version="1.1" class="slurve-definitions" xmlns="http://www.w3.org/2000/svg" preserveAspectRatio="none" width="600" height="400" style="position: absolute; max-width: 100%;">
    <defs>
        <pattern id="slurvePattern" x="0" y="0" width="400" height="400" patternUnits="userSpaceOnUse">
            <image xlink:href="http://haven.ellpreview.com/wp-content/themes/haven/img/global/sayagata-400px.png" x="0" y="0" width="400"
                height="400" />
        </pattern>
    </defs>
</svg>

<section class="HamburgerContainer box">
  <section class="HamburgerMenuTitle">
    Menu
  </section>
  <section class="Hamburger">
    <span class="line"></span>
    <span class="line"></span>
    <span class="line"></span>
  </section>
</section>
<!--/Hamburger Menu-->

<section class="Navigation Mobile">
  <?php echo $_smarty_tpl->smarty->registered_objects['nav'][0]->displayMobileMenu(array(),$_smarty_tpl);?>

</section>
<?php }
}
