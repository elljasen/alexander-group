<?php
/* Smarty version 3.1.30, created on 2017-08-16 20:02:26
  from "/Users/jasenpeterson/Sites/tag/wp-content/themes/TAG/smarty_templates/global/blogLayout.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_5994a4d22da897_01620437',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '83d835fd7e72766dad8cd3b188597d82185ab788' => 
    array (
      0 => '/Users/jasenpeterson/Sites/tag/wp-content/themes/TAG/smarty_templates/global/blogLayout.tpl',
      1 => 1502913743,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:../global/twitterFeed.tpl' => 2,
  ),
),false)) {
function content_5994a4d22da897_01620437 (Smarty_Internal_Template $_smarty_tpl) {
?>
<section class="blogLayout blogLayoutParent FlexContainer Horizontal">

  <div class="column WhiteBackgroundBlock"></div>

  <section class="column blogContent">
    <section class="box">

        <!--articlesToggle-->
        <section class="articlesToggle FlexContainer Horizontal">
        
          
          
        
        
          
          
        
        </section>
        <!--/articlesToggle-->

        <!-- PostContainer -->
        <section class="PostContainerWrap active post">
            <section class="PostContainer">

                <section class="FeaturedPostContainer FlexContainer Horizontal ColumnVertical">

                    <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['BlogPost']->value['post'], 'Post', false, NULL, 'blogpost', array (
  'iteration' => true,
));
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['Post']->value) {
$_smarty_tpl->tpl_vars['__smarty_foreach_blogpost']->value['iteration']++;
?>
                        <?php if ((isset($_smarty_tpl->tpl_vars['__smarty_foreach_blogpost']->value['iteration']) ? $_smarty_tpl->tpl_vars['__smarty_foreach_blogpost']->value['iteration'] : null) <= 2) {?>
                            <article class="column">
                                <a href="<?php echo $_smarty_tpl->tpl_vars['Post']->value['URL'];?>
">

                                    <section class="image BackgroundImage" style="background: url(<?php echo $_smarty_tpl->tpl_vars['Post']->value['Image'];?>
)"></section>

                                    <section class="PostContent">

                                        <h1 class="title"><?php echo $_smarty_tpl->tpl_vars['Post']->value['Title'];?>
</h1>

                                        <section class="PostMeta">
                                            <span class="PostAuthor">by: <?php echo $_smarty_tpl->tpl_vars['Post']->value['Author'];?>
 </span>
                                            <span class="PostDate"><?php echo $_smarty_tpl->tpl_vars['Post']->value['Date'];?>
</span>
                                        </section>

                                    </section>

                                </a>
                            </article>
                        <?php }?>
                    <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl);
?>

                </section>
                <!--/FeaturedPostContainer-->

                <section class="PostContainer PostList FlexContainer Horizontal ColumnVertical">
                    <section class="column">
                        <nav data-post-type="post" class="navigation pagination"><?php echo $_smarty_tpl->tpl_vars['Pagination']->value;?>
</nav>
                        <section class="AjaxLoader" style="opacity: 0; background: none; display: none; height: 100%;
                        position: absolute; top: 0; left: 0; align-items: flex-start">
                            <img class="ajax-loader" src="<?php echo $_smarty_tpl->tpl_vars['themeURL']->value;?>
/assets/images/icons/rolling.svg" alt="" style="margin: 10rem auto;">
                        </section>
                        <section class="articleList" style="position: relative;">
                            <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['BlogPost']->value['post'], 'Post', false, NULL, 'blogpost', array (
  'iteration' => true,
));
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['Post']->value) {
$_smarty_tpl->tpl_vars['__smarty_foreach_blogpost']->value['iteration']++;
?>
                                <?php if ((isset($_smarty_tpl->tpl_vars['__smarty_foreach_blogpost']->value['iteration']) ? $_smarty_tpl->tpl_vars['__smarty_foreach_blogpost']->value['iteration'] : null) > 2) {?>
                                    <article>
                                        <a href="<?php echo $_smarty_tpl->tpl_vars['Post']->value['URL'];?>
" class="FlexContainer Horizontal">

                                            <section class="image column BackgroundImage" style="background: url(<?php echo $_smarty_tpl->tpl_vars['Post']->value['Image'];?>
)"></section>

                                            <section class="PostContent column">
                                                <section class="box">

                                                    <h1 class="title"><?php echo $_smarty_tpl->tpl_vars['Post']->value['Title'];?>
</h1>

                                                    <section class="PostMeta">
                                                        <span class="PostAuthor">by: <?php echo $_smarty_tpl->tpl_vars['Post']->value['Author'];?>
 </span>
                                                        <span class="PostDate"><?php echo $_smarty_tpl->tpl_vars['Post']->value['Date'];?>
</span>
                                                    </section>

                                                </section>
                                            </section>

                                        </a>
                                    </article>
                                <?php }?>
                            <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl);
?>

                        </section>
                    </section>
                    <section class="column TwitterContainer">
                        <?php $_smarty_tpl->_subTemplateRender("file:../global/twitterFeed.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('TwitterFeed'=>$_smarty_tpl->tpl_vars['TwitterFeed']->value), 0, false);
?>

                    </section>
                </section>

            </section>
            <!-- /PostContainer -->

        </section>

        <section class="PostContainerWrap tag_news">
            <!-- PostContainer -->
            <section class="PostContainer">

                <section class="FeaturedPostContainer FlexContainer Horizontal ColumnVertical">
                    <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['BlogPost']->value['tag_news'], 'Post', false, NULL, 'blogpost', array (
  'iteration' => true,
));
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['Post']->value) {
$_smarty_tpl->tpl_vars['__smarty_foreach_blogpost']->value['iteration']++;
?>
                        <?php if ((isset($_smarty_tpl->tpl_vars['__smarty_foreach_blogpost']->value['iteration']) ? $_smarty_tpl->tpl_vars['__smarty_foreach_blogpost']->value['iteration'] : null) <= 2) {?>
                            <article class="column">
                                <a href="<?php echo $_smarty_tpl->tpl_vars['Post']->value['URL'];?>
">

                                    <section class="image BackgroundImage" style="background: url(<?php echo $_smarty_tpl->tpl_vars['Post']->value['Image'];?>
)"></section>

                                    <section class="PostContent">

                                        <h1 class="title"><?php echo $_smarty_tpl->tpl_vars['Post']->value['Title'];?>
</h1>

                                        <section class="PostMeta">
                                            <span class="PostAuthor">by: <?php echo $_smarty_tpl->tpl_vars['Post']->value['Author'];?>
 </span>
                                            <span class="PostDate"><?php echo $_smarty_tpl->tpl_vars['Post']->value['Date'];?>
</span>
                                        </section>

                                    </section>

                                </a>
                            </article>
                        <?php }?>
                    <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl);
?>


                </section>
                <!--/FeaturedPostContainer-->

                <section class="PostContainer PostList FlexContainer Horizontal ColumnVertical">
                    <section class="column articleList">

                        <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['BlogPost']->value['post'], 'Post', false, NULL, 'blogpost', array (
  'iteration' => true,
));
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['Post']->value) {
$_smarty_tpl->tpl_vars['__smarty_foreach_blogpost']->value['iteration']++;
?>
                            <?php if ((isset($_smarty_tpl->tpl_vars['__smarty_foreach_blogpost']->value['iteration']) ? $_smarty_tpl->tpl_vars['__smarty_foreach_blogpost']->value['iteration'] : null) > 2) {?>
                                <article>
                                    <a href="<?php echo $_smarty_tpl->tpl_vars['Post']->value['URL'];?>
" class="FlexContainer Horizontal">

                                        <section class="image column BackgroundImage" style="background: url(<?php echo $_smarty_tpl->tpl_vars['Post']->value['Image'];?>
)"></section>

                                        <section class="PostContent column">
                                            <section class="box">

                                                <h1 class="title"><?php echo $_smarty_tpl->tpl_vars['Post']->value['Title'];?>
</h1>

                                                <section class="PostMeta">
                                                    <span class="PostAuthor">by: <?php echo $_smarty_tpl->tpl_vars['Post']->value['Author'];?>
 </span>
                                                    <span class="PostDate"><?php echo $_smarty_tpl->tpl_vars['Post']->value['Date'];?>
</span>
                                                </section>

                                            </section>
                                        </section>

                                    </a>
                                </article>
                            <?php }?>
                        <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl);
?>

                        <nav data-post-type="tag_news" class="paginationNav"><?php echo $_smarty_tpl->tpl_vars['Pagination']->value;?>
</nav>
                    </section>
                    <section class="column TwitterContainer">
                        <?php $_smarty_tpl->_subTemplateRender("file:../global/twitterFeed.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('TwitterFeed'=>$_smarty_tpl->tpl_vars['TwitterFeed']->value), 0, true);
?>

                    </section>
                </section>

            </section>
            <!-- /PostContainer -->
        </section>

    </section>
  </section>

</section>
<?php }
}
