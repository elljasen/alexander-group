<?php
/* Smarty version 3.1.30, created on 2017-06-13 18:44:55
  from "/Users/jasenpeterson/Sites/tag/wp-content/themes/TAG/smarty_templates/pages/home.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_594032a7b22d68_37709705',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '4b6db1046dda40da2a67b46add507b1c414ec781' => 
    array (
      0 => '/Users/jasenpeterson/Sites/tag/wp-content/themes/TAG/smarty_templates/pages/home.tpl',
      1 => 1497379493,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:../global/pageBanner.tpl' => 1,
    'file:../portals/aboutPortal.tpl' => 1,
    'file:../portals/expertisePortal.tpl' => 1,
    'file:../portals/locationsPortal.tpl' => 1,
    'file:../global/publications.tpl' => 1,
  ),
),false)) {
function content_594032a7b22d68_37709705 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_assignInScope('NewsList', $_smarty_tpl->tpl_vars['NewPostObject']->value);
$_smarty_tpl->_subTemplateRender("file:../global/pageBanner.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('Banner'=>$_smarty_tpl->tpl_vars['PageBanner']->value,'BannerClass'=>'Home'), 0, false);
?>

<?php $_smarty_tpl->_subTemplateRender("file:../portals/aboutPortal.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('AboutPortal'=>$_smarty_tpl->tpl_vars['AboutPagePortal']->value), 0, false);
?>

<?php $_smarty_tpl->_subTemplateRender("file:../portals/expertisePortal.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('ExpertisePortal'=>$_smarty_tpl->tpl_vars['ExpertisePagePortal']->value), 0, false);
?>

<?php $_smarty_tpl->_subTemplateRender("file:../portals/locationsPortal.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('LocationsPortal'=>$_smarty_tpl->tpl_vars['LocationsPagePortal']->value), 0, false);
?>

<?php $_smarty_tpl->_subTemplateRender("file:../global/publications.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('PublicationsPostObject'=>$_smarty_tpl->tpl_vars['Publications']->value), 0, false);
?>

<?php }
}
