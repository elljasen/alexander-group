<?php
/* Smarty version 3.1.30, created on 2017-06-15 16:58:53
  from "/Users/jasenpeterson/Sites/tag/wp-content/themes/TAG/smarty_templates/pages/leadership.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_5942bccd699b25_12796741',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'b79285eefcf3874626a512dee64ba0485fbb0731' => 
    array (
      0 => '/Users/jasenpeterson/Sites/tag/wp-content/themes/TAG/smarty_templates/pages/leadership.tpl',
      1 => 1497479706,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:../global/pageBanner.tpl' => 1,
    'file:../global/cards.tpl' => 1,
  ),
),false)) {
function content_5942bccd699b25_12796741 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_subTemplateRender("file:../global/pageBanner.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('Banner'=>$_smarty_tpl->tpl_vars['TeamPageBanner']->value,'BannerClass'=>'Team Interior'), 0, false);
?>

<?php $_smarty_tpl->_subTemplateRender("file:../global/cards.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('Cards'=>$_smarty_tpl->tpl_vars['TeamProfiles']->value,'CardClass'=>'TeamProfiles'), 0, false);
?>

<?php }
}
