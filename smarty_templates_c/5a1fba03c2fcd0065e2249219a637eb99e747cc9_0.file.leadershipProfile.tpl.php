<?php
/* Smarty version 3.1.30, created on 2017-08-08 18:06:00
  from "/Users/jasenpeterson/Sites/tag/wp-content/themes/TAG/smarty_templates/global/leadershipProfile.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_5989fd889a1451_97181978',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '5a1fba03c2fcd0065e2249219a637eb99e747cc9' => 
    array (
      0 => '/Users/jasenpeterson/Sites/tag/wp-content/themes/TAG/smarty_templates/global/leadershipProfile.tpl',
      1 => 1502215556,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5989fd889a1451_97181978 (Smarty_Internal_Template $_smarty_tpl) {
?>
<section class="AjaxLoader">
  <img src="<?php echo $_smarty_tpl->tpl_vars['themeURL']->value;?>
/assets/images/icons/rolling.svg" alt="">
  <section class="TeamProfileFull AjaxContainer">
    <section class="CloseButton">
      <svg xmlns="http://www.w3.org/2000/svg" id="Layer_1" viewBox="0 0 640 480" class="AjaxCloseButton">
        <path d="M319.4 126C256.2 126 205 177.2 205 240.4s51.2 114.4 114.4 114.4 114.4-51.2 114.4-114.4S382.6 126 319.4 126zm0 207.4c-51.3 0-93-41.6-93-93 0-51.3 41.6-93 93-93s93 41.6 93 93-41.6 93-93 93zm20.3-93l20.2-20.2c2.8-2.8 2.8-7.3 0-10.1L349.8 200c-2.8-2.8-7.3-2.8-10.1 0l-20.2 20.2-20.2-20.2c-2.8-2.8-7.3-2.8-10.1 0L279 210c-2.8 2.8-2.8 7.3 0 10.1l20.2 20.2-20.2 20.2c-2.8 2.8-2.8 7.3 0 10.1l10.1 10.1c2.8 2.8 7.3 2.8 10.1 0l20.2-20.2 20.3 20.3c2.8 2.8 7.3 2.8 10.1 0l10.1-10.1c2.8-2.8 2.8-7.3 0-10.1l-20.2-20.2zm0 0" class="st0"/>
      </svg>
    </section>
    <section class="FlexContainer">
      <section class="column Name">
        <section class="box">
          <h1></h1>
        </section>
      </section>
      <section class="FlexContainer Horizontal ColumnVertical">
        <section class="column Image"></section>
        <section class="column Content">
          <h1></h1>
          <section class="Description"></section>
          <a class="btn" href="">
            <span>Learn More</span>
            <div class="line"></div>
          </a>
        </section>
      </section>
      <section class="FlexContainer Experience">
        <section class="column ExperienceLabel">
          <section class="box">
            <h1></h1>
          </section>
        </section>
        <section class="column">
          <section class="FlexContainer Horizontal">
            <ul class="ExperienceList">

            </ul>
          </section>
        </section>
      </section>
    </section>
  </section>
</section>
<?php }
}
