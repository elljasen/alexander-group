<?php
/* Smarty version 3.1.30, created on 2017-06-05 17:29:22
  from "/Users/jasenpeterson/Sites/haven/wp-content/themes/haven/smarty_templates/pages/about.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_593594f2769c26_72024971',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '938862ae28e92152fa8297f20a5d879909b53e2c' => 
    array (
      0 => '/Users/jasenpeterson/Sites/haven/wp-content/themes/haven/smarty_templates/pages/about.tpl',
      1 => 1496683760,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:../global/pageBanner.tpl' => 1,
  ),
),false)) {
function content_593594f2769c26_72024971 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_subTemplateRender("file:../global/pageBanner.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('PageBannerImage'=>$_smarty_tpl->tpl_vars['PageBanner']->value['url'],'PageSlug'=>'about-haven'), 0, false);
?>

<section class="InteriorPage about-haven">
  <section class="AboutPage Intro">
    <section class="container">
      <section class="grid">
        <section class="row row-align-center">
          <section class="gr-6 gr-12@xs gr-12@sm">
            <section class="Image">
              <img src="<?php echo $_smarty_tpl->tpl_vars['AboutHaven']->value[0]['introduction'][0]['image']['url'];?>
" alt="<?php echo $_smarty_tpl->tpl_vars['AboutHaven']->value[0]['introduction'][0]['image']['alt'];?>
">
            </section>
          </section>
          <section class="gr-6 gr-12@xs gr-12@sm">
            <section class="box AboutContent">
              <svg class="Diamond TwoDiamonds" width="32px" height="32px" viewBox="0 0 40 32" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                  <polygon points="16,2 30,16 16,30 2,16"></polygon>
                  <polygon points="16,2 30,16 16,30 2,16"></polygon>
              </svg>
              <h1><?php echo $_smarty_tpl->tpl_vars['AboutHaven']->value[0]['introduction'][0]['title'];?>
</h1>
              <?php echo $_smarty_tpl->tpl_vars['AboutHaven']->value[0]['introduction'][0]['content'];?>

            </section>
          </section>
        </section>
      </section>
    </section>
  </section>
  <section class="AboutPage ContentBreakImage">
    <img src="<?php echo $_smarty_tpl->tpl_vars['AboutHaven']->value[0]['content_break_image']['url'];?>
" alt="<?php echo $_smarty_tpl->tpl_vars['AboutHaven']->value[0]['content_break_image']['alt'];?>
">
  </section>
  <section class="AboutPage Content">
    <section class="container">
      <section class="grid">
        <section class="row row-align-center">
          <section class="gr-10 gr-12@xs gr-12@sm">
            <section class="box">
              <?php echo $_smarty_tpl->tpl_vars['AboutHaven']->value[0]['content'];?>

            </section>
          </section>
        </section>
      </section>
    </section>
  </section>
</section>
<?php }
}
