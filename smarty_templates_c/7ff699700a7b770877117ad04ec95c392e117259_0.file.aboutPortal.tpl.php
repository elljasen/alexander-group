<?php
/* Smarty version 3.1.30, created on 2017-06-15 17:28:25
  from "/Users/jasenpeterson/Sites/tag/wp-content/themes/TAG/smarty_templates/portals/aboutPortal.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_5942c3b9030535_37618007',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '7ff699700a7b770877117ad04ec95c392e117259' => 
    array (
      0 => '/Users/jasenpeterson/Sites/tag/wp-content/themes/TAG/smarty_templates/portals/aboutPortal.tpl',
      1 => 1497547699,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:../global/newsList.tpl' => 1,
  ),
),false)) {
function content_5942c3b9030535_37618007 (Smarty_Internal_Template $_smarty_tpl) {
if (!empty($_smarty_tpl->tpl_vars['AboutPortal']->value)) {?>

<section class="FlexContainer AboutPortalFlexContainer">

  <sectin class="row">

    <section class="column PagePortal">

      <section class="FlexContainer">

        <section class="row">
          <div class="column LightBlueBackgroundBlock"></div>
          <section class="column BackgroundImage" style="background: url(<?php echo $_smarty_tpl->tpl_vars['AboutPortal']->value['pagePortal'][0]['image']['url'];?>
) no-repeat"></section>
          <section class="column Content">
            <section class="box PortalContent">
              <h1><?php echo $_smarty_tpl->tpl_vars['AboutPortal']->value['pagePortal'][0]['title'];?>
 &ndash;</h1>
              <?php echo $_smarty_tpl->tpl_vars['AboutPortal']->value['pagePortal'][0]['content'];?>

              <a class="btn" href="<?php echo $_smarty_tpl->tpl_vars['homeURL']->value;?>
/<?php echo $_smarty_tpl->tpl_vars['AboutPortal']->value['slug'];?>
">
                <span>Learn More</span>
                <div class="line"></div>
              </a>
            </section>
          </section>
          <section class="column CompanyMotto FullColumn">
            <section class="box">
              <h1 class="HeroText"><?php echo $_smarty_tpl->tpl_vars['CompanyMotto']->value;?>
</h1>
            </section>
          </section>
        </section>

      </section>

    </section>

    <section class="column NewsList">
      <?php $_smarty_tpl->_subTemplateRender("file:../global/newsList.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

    </section>

    <div class="column DarkBlueBackgroundBlock"></div>

  </section>
  <!--/.row-->

</section>
<?php }
}
}
