<?php
/* Smarty version 3.1.30, created on 2017-08-09 19:49:33
  from "/Users/jasenpeterson/Sites/tag/wp-content/themes/TAG/smarty_templates/pages/single.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_598b674decd075_53039989',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '326376406c683a497938753f54c4d84dc1716dd0' => 
    array (
      0 => '/Users/jasenpeterson/Sites/tag/wp-content/themes/TAG/smarty_templates/pages/single.tpl',
      1 => 1502308173,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:../global/recentPost.tpl' => 1,
  ),
),false)) {
function content_598b674decd075_53039989 (Smarty_Internal_Template $_smarty_tpl) {
?>
<section class="blogLayout blogLayoutChild FlexContainer Horizontal">

  <div class="column WhiteBackgroundBlock"></div>

  <section class="column blogContent">
    <section class="box">
      <section class="blogBreadcrumb">
          <?php echo the_breadcrumb();?>

      </section>
      <section class="PostContainer">

        <section class="BackgroundImage" style="background: url(<?php echo $_smarty_tpl->tpl_vars['SinglePostImage']->value;?>
)">
          
          <section class="SinglePostTitle">
            <section class="box">

                <h1><?php echo $_smarty_tpl->tpl_vars['SinglePost']->value->post_title;?>
</h1>

                <section class="PostMeta">
                  <span class="PostAuthor">by: <?php echo $_smarty_tpl->tpl_vars['SinglePostAuthor']->value;?>
 </span>
                  <span class="PostDate"><?php echo $_smarty_tpl->tpl_vars['SinglePostDate']->value;?>
</span>
                </section>

            </section>
          </section>
        </section>

        <section class="FlexContainer Horizontal ColumnVertical SinglePostContainer">
          <section class="column SinglePostContent">
            <?php echo $_smarty_tpl->tpl_vars['SinglePostContent']->value;?>

          </section>
          <section class="column RecentPost">
            <?php $_smarty_tpl->_subTemplateRender("file:../global/recentPost.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('RecentPost'=>$_smarty_tpl->tpl_vars['RecentPost']->value), 0, false);
?>

          </section>
        </section>

      </section>
    </section>
  </section>

</section>
<?php }
}
