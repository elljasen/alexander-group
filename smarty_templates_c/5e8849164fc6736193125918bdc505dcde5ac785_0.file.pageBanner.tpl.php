<?php
/* Smarty version 3.1.30, created on 2017-06-04 16:22:37
  from "/Users/jasenpeterson/Sites/haven/wp-content/themes/haven/smarty_templates/global/pageBanner.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_593433cd550573_74613763',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '5e8849164fc6736193125918bdc505dcde5ac785' => 
    array (
      0 => '/Users/jasenpeterson/Sites/haven/wp-content/themes/haven/smarty_templates/global/pageBanner.tpl',
      1 => 1496593356,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_593433cd550573_74613763 (Smarty_Internal_Template $_smarty_tpl) {
if (!empty($_smarty_tpl->tpl_vars['PageBannerImage']->value)) {?>
<section class="PageBanner <?php echo $_smarty_tpl->tpl_vars['PageSlug']->value;?>
" style="background: url(<?php echo $_smarty_tpl->tpl_vars['PageBannerImage']->value;?>
)">
    <section class="tint"></section>
</section>
<?php }
}
}
