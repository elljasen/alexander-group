<?php
/* Smarty version 3.1.30, created on 2017-08-08 16:43:59
  from "/Users/jasenpeterson/Sites/tag/wp-content/themes/TAG/smarty_templates/global/publications.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_5989ea4f48c479_71252039',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '420191a9ec69747683c2f712948ad569302d40e4' => 
    array (
      0 => '/Users/jasenpeterson/Sites/tag/wp-content/themes/TAG/smarty_templates/global/publications.tpl',
      1 => 1502210636,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5989ea4f48c479_71252039 (Smarty_Internal_Template $_smarty_tpl) {
?>

<section class="PublicationsFlexContainer FlexContainer">
  <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['Publications']->value['publication'], 'Publication');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['Publication']->value) {
?>
    <section class="column">
      <section class="box Publication">
        <a href="<?php echo $_smarty_tpl->tpl_vars['Publication']->value['URL'];?>
">
          <section class="inner">
            <span class="PostDate"><?php echo $_smarty_tpl->tpl_vars['Publication']->value['Date'];?>
</span>
            <h1><?php echo $_smarty_tpl->tpl_vars['Publication']->value['Title'];?>
</h1>
              <?php echo $_smarty_tpl->tpl_vars['Publication']->value['Publications']['Excerpt'];?>

            <img src="<?php echo $_smarty_tpl->tpl_vars['Publication']->value['Publications']['Logo']['url'];?>
" alt="<?php echo $_smarty_tpl->tpl_vars['Publication']->value['Publications']['Logo']['alt'];?>
" />
          </section>
        </a>
      </section>
    </section>
  <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl);
?>

</section>
<?php }
}
