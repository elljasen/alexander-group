<?php
/* Smarty version 3.1.30, created on 2017-08-10 21:06:40
  from "/Users/jasenpeterson/Sites/tag/wp-content/themes/TAG/smarty_templates/pages/about.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_598ccae0051965_71372756',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '7ffbc99b9ccdb9c04c3f61b6b6ac443b99a849e4' => 
    array (
      0 => '/Users/jasenpeterson/Sites/tag/wp-content/themes/TAG/smarty_templates/pages/about.tpl',
      1 => 1502399191,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:../global/pageBanner.tpl' => 1,
    'file:../global/videoBlock.tpl' => 1,
  ),
),false)) {
function content_598ccae0051965_71372756 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_subTemplateRender("file:../global/pageBanner.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('Banner'=>$_smarty_tpl->tpl_vars['AboutPageBanner']->value,'BannerClass'=>'About Interior'), 0, false);
?>


<section class="AboutPageIntro FlexContainer">
  <div class="column LightBlueBackgroundBlock"></div>

  <section class="column">

    <section class="box Content">

      <section class="FlexContainer">

        <section class="column">
          <section class="box HeroText">
            <h1><?php echo $_smarty_tpl->tpl_vars['AboutPageIntro']->value[0]['hero_text'];?>
</h1>
          </section>
        </section>

        <section class="column ContentModules">
          <section class="ContentModule box">
            <h1><?php echo $_smarty_tpl->tpl_vars['AboutPageIntro']->value[0]['hero_content_title'];?>
 &ndash;</h1>
            <?php echo $_smarty_tpl->tpl_vars['AboutPageIntro']->value[0]['hero_content'];?>

          </section>
        </section>

      </section>

      <section class="FlexContainer Horizontal">
        <section class="column DarkGrayBackground">
          <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['AboutPageIntro']->value[1]['content_module'], 'Content');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['Content']->value) {
?>
            <section class="ContentModule box">
              <h1><?php echo $_smarty_tpl->tpl_vars['Content']->value['title'];?>
 &ndash;</h1>
              <?php echo $_smarty_tpl->tpl_vars['Content']->value['content'];?>

            </section>
          <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl);
?>

        </section>
        <section class="column BackgroundImage" style="background: url(<?php echo $_smarty_tpl->tpl_vars['AboutPageIntro']->value[1]['image']['url'];?>
)"></section>
      </section>

    </section>

  </section>

  <div class="column DarkBlueBackgroundBlock"></div>
</section>

<section class="OurCulture">
  <?php $_smarty_tpl->_subTemplateRender("file:../global/videoBlock.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('Video'=>$_smarty_tpl->tpl_vars['Video']->value,'Class'=>"Interior"), 0, false);
?>

  <section class="OurCultureContent  min-height FlexContainer">

    <div class="column DarkBlueBackgroundBlock"></div>

    <section class="column ColumnOne min-height">

      <section class="FlexContainer Horizontal min-height">

        <div class="column DarkBlueBackgroundBlock min-height"></div>

        <section class="column DarkGrayBackground min-height OurCultureContentBox">
          <section class="ContentModule box">
            <h1 class="HeroText"><?php echo $_smarty_tpl->tpl_vars['OurCulture']->value[0]['hero_text'];?>
</h1>
            <h1><?php echo $_smarty_tpl->tpl_vars['OurCulture']->value[0]['title'];?>
 &ndash;</h1>
            <?php echo $_smarty_tpl->tpl_vars['OurCulture']->value[0]['content'];?>

          </section>
        </section>

        <section class="column BabyBlueBackground min-height">

        </section>

      </section>

    </section>

    <section class="column ColumnTwo" style="min-height: 450px;">
      <section class="FlexContainer Horizontal" style="min-height: 450px;">
        <section class="column" style="min-height: 450px;">
          <section class="ContentModule box">
            <h1><?php echo $_smarty_tpl->tpl_vars['OurCulture']->value[1]['title'];?>
 &ndash;</h1>
            <?php echo $_smarty_tpl->tpl_vars['OurCulture']->value[1]['content'];?>

          </section>
        </section>
      </section>
    </section>

  </section>
</section>
<?php }
}
