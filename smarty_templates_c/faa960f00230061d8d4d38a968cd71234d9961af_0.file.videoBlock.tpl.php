<?php
/* Smarty version 3.1.30, created on 2017-06-14 20:05:31
  from "/Users/jasenpeterson/Sites/tag/wp-content/themes/TAG/smarty_templates/global/videoBlock.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_5941970bea7203_63550829',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'faa960f00230061d8d4d38a968cd71234d9961af' => 
    array (
      0 => '/Users/jasenpeterson/Sites/tag/wp-content/themes/TAG/smarty_templates/global/videoBlock.tpl',
      1 => 1497470728,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5941970bea7203_63550829 (Smarty_Internal_Template $_smarty_tpl) {
?>
<section class="VideoBlock PageBanner <?php echo $_smarty_tpl->tpl_vars['Class']->value;?>
" style="background:url(<?php echo $_smarty_tpl->tpl_vars['Video']->value[0]['poster']['url'];?>
)">
  <h1 class="Flex">
    <span><?php echo $_smarty_tpl->tpl_vars['Video']->value[0]['title'];?>
 &ndash;</span>
    <svg xmlns="http://www.w3.org/2000/svg" class="PlayButtonIcon" viewBox="0 0 410 411">
      <path d="M284.6 198.1l-108.1-73.9c-3.4-2.5-12.3-2.5-13 6.9v147.8c.8 9.4 9.9 9.5 13 6.9l108.1-73.9c2.8-1.7 6.6-8.6 0-13.8zm-104.4 65.1V146.8L265 205l-84.8 58.2zm0 0" class="st0"/>
      <path d="M206 9C97.9 9 10 96.9 10 205s87.9 196 196 196 196-87.9 196-196S314.1 9 206 9zm0 375.3c-98.9 0-179.3-80.4-179.3-179.3S107.1 25.7 206 25.7 385.3 106.1 385.3 205 304.9 384.3 206 384.3zm0 0" class="st0"/>
    </svg>
  </h1>
</section>
<?php }
}
