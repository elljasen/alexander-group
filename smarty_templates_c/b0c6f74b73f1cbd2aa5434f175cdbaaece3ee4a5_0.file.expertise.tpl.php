<?php
/* Smarty version 3.1.30, created on 2017-06-29 14:20:42
  from "/Users/jasenpeterson/Sites/tag/wp-content/themes/TAG/smarty_templates/pages/expertise.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_59550cbad78195_90704317',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'b0c6f74b73f1cbd2aa5434f175cdbaaece3ee4a5' => 
    array (
      0 => '/Users/jasenpeterson/Sites/tag/wp-content/themes/TAG/smarty_templates/pages/expertise.tpl',
      1 => 1498746041,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:../global/pageBanner.tpl' => 1,
  ),
),false)) {
function content_59550cbad78195_90704317 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_subTemplateRender("file:../global/pageBanner.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('Banner'=>$_smarty_tpl->tpl_vars['ExpertiseBanner']->value,'BannerClass'=>'Expertise Interior'), 0, false);
?>


<section class="ExpertiseBannerContent">
  <section class="box">
    <?php echo $_smarty_tpl->tpl_vars['ExpertiseBannerContent']->value;?>

  </section>
</section>

<section class="ExpertiseIntro Horizontal FlexContainer">

  <!-- COLUMN -->
  <section class="column">

    <!--ROW-->
    <section class="row RowOne">

      <!--COLUMN-->
      <section class="column HeroText">
        <section class="box">
          <?php echo $_smarty_tpl->tpl_vars['ExpertiseIntro']->value[0]['column_one'][0]['content'];?>

        </section>
      </section>
      <!--/COLUMN-->

      <!--COLUMN-->
      <section class="column">
        <section class="box">
          <h1><?php echo $_smarty_tpl->tpl_vars['ExpertiseIntro']->value[0]['column_two'][0]['label'];?>
 &ndash;</h1>
          <?php echo $_smarty_tpl->tpl_vars['ExpertiseIntro']->value[0]['column_two'][0]['content'];?>

        </section>
      </section>
      <!--/COLUMN-->

    </section>
    <!--/ROW-->

    <!--ROW-->
    <section class="row RowTwo">

      <!--COLUMN-->
      <section class="column">
        <section class="box">
          <h1><?php echo $_smarty_tpl->tpl_vars['ExpertiseIntro']->value[1]['column_one'][0]['label'];?>
 &ndash;</h1>
          <?php echo $_smarty_tpl->tpl_vars['ExpertiseIntro']->value[1]['column_one'][0]['content'];?>

        </section>
      </section>
      <!--/COLUMN-->

      <!--COLUMN-->
      <section class="column">
        <section class="box">
          <?php echo $_smarty_tpl->tpl_vars['ExpertiseIntro']->value[1]['column_two'][0]['content'];?>

        </section>
        <section class="BackgroundImage" style="background: url(<?php echo $_smarty_tpl->tpl_vars['ExpertiseIntro']->value[1]['column_two'][0]['image']['url'];?>
)"></section>
      </section>
      <!--/COLUMN-->

    </section>
    <!--/ROW-->

  </section>
  <!--/COLUMN-->

  <div class="column LightBlueBackgroundBlock"></div>

</section>

<section class="ExpertisePageBreak FlexContainer" style="background: url(<?php echo $_smarty_tpl->tpl_vars['ExpertisePageBreakImage']->value['url'];?>
)">
  <section class="row">
    <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['ExpertisePageBreak']->value[0]['column'], 'PageBreak');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['PageBreak']->value) {
?>
    <section class="column">
      <h1><?php echo $_smarty_tpl->tpl_vars['PageBreak']->value['Stat'];?>
</h1>
      <h2><?php echo $_smarty_tpl->tpl_vars['PageBreak']->value['label'];?>
</h2>
      <?php echo $_smarty_tpl->tpl_vars['PageBreak']->value['content'];?>

    </section>
    <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl);
?>

  </section>
</section>
<?php }
}
