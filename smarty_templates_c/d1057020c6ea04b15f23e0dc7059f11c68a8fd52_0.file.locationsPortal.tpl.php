<?php
/* Smarty version 3.1.30, created on 2017-06-15 15:09:03
  from "/Users/jasenpeterson/Sites/tag/wp-content/themes/TAG/smarty_templates/portals/locationsPortal.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_5942a30f2f5781_09737672',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'd1057020c6ea04b15f23e0dc7059f11c68a8fd52' => 
    array (
      0 => '/Users/jasenpeterson/Sites/tag/wp-content/themes/TAG/smarty_templates/portals/locationsPortal.tpl',
      1 => 1497539341,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5942a30f2f5781_09737672 (Smarty_Internal_Template $_smarty_tpl) {
if (!empty($_smarty_tpl->tpl_vars['LocationsPortal']->value)) {?>

<section class="FlexContainer LocationsPortalFlexContainer PagePortal">

  <section class="row">

    <section class="column Content">
      <section class="box PortalContent">
        <section class="inner">
          <h1><?php echo $_smarty_tpl->tpl_vars['LocationsPortal']->value['pagePortal'][0]['title'];?>
 &ndash;</h1>
          <?php echo $_smarty_tpl->tpl_vars['LocationsPortal']->value['pagePortal'][0]['content'];?>

        </section>=
      </section>
      <div class="column LightBlueBackgroundBlock"></div>
      <section class="column LocationsList">
        <section class="box">
          <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['Locations']->value, 'location');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['location']->value) {
?>
            <h1><?php echo $_smarty_tpl->tpl_vars['location']->value['location'];?>
</h1>
          <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl);
?>

          <a class="btn btn-reverse" href="<?php echo $_smarty_tpl->tpl_vars['homeURL']->value;?>
/<?php echo $_smarty_tpl->tpl_vars['LocationsPortal']->value['slug'];?>
">
            <span>View all our locations</span>
            <div class="line"></div>
          </a>
        </section>
      </section>
    </section>

    <section class="column BackgroundImage" style="background: url(<?php echo $_smarty_tpl->tpl_vars['LocationsPortal']->value['pagePortal'][0]['image']['url'];?>
) no-repeat"></section>

  </section>

</section>

<?php }
}
}
