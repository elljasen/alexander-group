<?php
/* Smarty version 3.1.30, created on 2017-06-02 22:11:07
  from "/Users/jasenpeterson/Sites/haven/wp-content/themes/haven/smarty_templates/global/pagePortal.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_5931e27be380a7_54995482',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'a68336fc1d5440d87a7a67cb184ee306ffec1ef4' => 
    array (
      0 => '/Users/jasenpeterson/Sites/haven/wp-content/themes/haven/smarty_templates/global/pagePortal.tpl',
      1 => 1496441465,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5931e27be380a7_54995482 (Smarty_Internal_Template $_smarty_tpl) {
if (!is_callable('smarty_modifier_replace')) require_once '/Users/jasenpeterson/Sites/haven/wp-content/themes/haven/smarty/libs/plugins/modifier.replace.php';
?>

<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['PagePortals']->value, 'i', false, 'PortalName');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['PortalName']->value => $_smarty_tpl->tpl_vars['i']->value) {
?>

<section class="PagePortal <?php echo smarty_modifier_replace($_smarty_tpl->tpl_vars['i']->value[0]['label'],' ','');?>
">
    <section class="container">

        
        <?php if ($_smarty_tpl->tpl_vars['i']->value[0]['label'] == 'Our Office') {?>

        <section class="PagePortalContent <?php echo smarty_modifier_replace($_smarty_tpl->tpl_vars['i']->value[0]['label'],' ','');?>
">

            <section class="Inner">
                <section class="Title <?php echo smarty_modifier_replace($_smarty_tpl->tpl_vars['i']->value[0]['label'],' ','');?>
">
                    <svg class="Diamond <?php echo smarty_modifier_replace($_smarty_tpl->tpl_vars['i']->value[0]['label'],' ','');?>
" width="32px" height="32px" viewBox="0 0 32 32" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                        <polygon points="16,2 30,16 16,30 2,16"></polygon>
                    </svg>
                    <h5><?php echo $_smarty_tpl->tpl_vars['i']->value[0]['label'];?>
</h5>
                </section>

                <section class="box">
                    <h1><?php echo $_smarty_tpl->tpl_vars['i']->value[0]['title'];?>
</h1>
                    <?php echo $_smarty_tpl->tpl_vars['i']->value[0]['content'];?>

                </section>

                <svg xmlns="http://www.w3.org/2000/svg" width="164" height="118" viewBox="0 0 164 118" class="line <?php echo smarty_modifier_replace($_smarty_tpl->tpl_vars['i']->value[0]['label'],' ','');?>
">
                    <line y1="118" x2="164" fill="none" />
                </svg>
            </section>

            <svg class="DiamondBig <?php echo smarty_modifier_replace($_smarty_tpl->tpl_vars['i']->value[0]['label'],' ','');?>
" width="32px" height="32px" viewBox="0 0 32 32" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                <polygon points="16,2 30,16 16,30 2,16"></polygon>
            </svg>

        </section>

        <section class="PagePortalImage">
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 827 709" class="Slurve <?php echo smarty_modifier_replace($_smarty_tpl->tpl_vars['i']->value[0]['label'],' ','');?>
">
                <path d="M-1 51.8L827 0v709.1H-1V51.8z"/>
            </svg>
            <img src="<?php echo $_smarty_tpl->tpl_vars['i']->value[0]['image']['url'];?>
" />
        </section>
        
        <?php } elseif ($_smarty_tpl->tpl_vars['i']->value[0]['label'] == 'Amenities') {?>

            <section class="PagePortalContent <?php echo smarty_modifier_replace($_smarty_tpl->tpl_vars['i']->value[0]['label'],' ','');?>
">

                <section class="Inner">
                    <section class="box">
                        <section class="Title <?php echo smarty_modifier_replace($_smarty_tpl->tpl_vars['i']->value[0]['label'],' ','');?>
">
                            <h5><?php echo $_smarty_tpl->tpl_vars['i']->value[0]['label'];?>
</h5>
                            <svg class="Diamond <?php echo smarty_modifier_replace($_smarty_tpl->tpl_vars['i']->value[0]['label'],' ','');?>
" width="32px" height="32px" viewBox="0 0 32 32" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                                <polygon points="16,2 30,16 16,30 2,16"></polygon>
                                <polygon points="16,2 30,16 16,30 2,16"></polygon>
                            </svg>
                        </section>
                        <h1><?php echo $_smarty_tpl->tpl_vars['i']->value[0]['title'];?>
</h1>
                        <?php echo $_smarty_tpl->tpl_vars['i']->value[0]['content'];?>

                    </section>

                    <svg xmlns="http://www.w3.org/2000/svg" width="164" height="118" viewBox="0 0 164 118" class="line <?php echo smarty_modifier_replace($_smarty_tpl->tpl_vars['i']->value[0]['label'],' ','');?>
">
                        <line y1="118" x2="164" fill="none" />
                    </svg>
                </section>

                <svg class="DiamondBig <?php echo smarty_modifier_replace($_smarty_tpl->tpl_vars['i']->value[0]['label'],' ','');?>
" width="32px" height="32px" viewBox="0 0 32 32" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                    <polygon points="16,2 30,16 16,30 2,16"></polygon>
                </svg>

            </section>

            <section class="PagePortalImage">
                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 827 400" class="Slurve <?php echo smarty_modifier_replace($_smarty_tpl->tpl_vars['i']->value[0]['label'],' ','');?>
">
                    <path d="M-1 51.8L827 0v709.1H-1V51.8z"/>
                </svg>
                <img src="<?php echo $_smarty_tpl->tpl_vars['i']->value[0]['image']['url'];?>
" />
            </section>

            
            <?php } elseif ($_smarty_tpl->tpl_vars['i']->value[0]['label'] == 'Our Services') {?>

                <section class="PagePortalContent <?php echo smarty_modifier_replace($_smarty_tpl->tpl_vars['i']->value[0]['label'],' ','');?>
">

                    <section class="Inner">

                        <section class="Title <?php echo smarty_modifier_replace($_smarty_tpl->tpl_vars['i']->value[0]['label'],' ','');?>
">
                            <h5><?php echo $_smarty_tpl->tpl_vars['i']->value[0]['label'];?>
</h5>
                        </section>

                        <section class="box">
                            <h1><?php echo $_smarty_tpl->tpl_vars['i']->value[0]['title'];?>
</h1>
                            <?php echo $_smarty_tpl->tpl_vars['i']->value[0]['content'];?>

                            <svg class="Diamond <?php echo smarty_modifier_replace($_smarty_tpl->tpl_vars['i']->value[0]['label'],' ','');?>
" width="32px" height="32px" viewBox="0 0 40 32" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                                <polygon points="16,2 30,16 16,30 2,16"></polygon>
                                <polygon points="16,2 30,16 16,30 2,16"></polygon>
                            </svg>
                        </section>

                    <svg xmlns="http://www.w3.org/2000/svg" width="164" height="118" viewBox="0 0 164 118" class="line <?php echo smarty_modifier_replace($_smarty_tpl->tpl_vars['i']->value[0]['label'],' ','');?>
">
                        <line y1="118" x2="164" fill="none" />
                    </svg>
                    </section>

                    <svg class="DiamondBig <?php echo smarty_modifier_replace($_smarty_tpl->tpl_vars['i']->value[0]['label'],' ','');?>
" width="32px" height="32px" viewBox="0 0 32 32" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                        <polygon points="16,2 30,16 16,30 2,16"></polygon>
                    </svg>

                </section>

                <section class="PagePortalImage">
                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 827 400" class="Slurve <?php echo smarty_modifier_replace($_smarty_tpl->tpl_vars['i']->value[0]['label'],' ','');?>
">
                        <path d="M-1 51.8L827 0v709.1H-1V51.8z"/>
                    </svg>
                    <img src="<?php echo $_smarty_tpl->tpl_vars['i']->value[0]['image']['url'];?>
" />
                </section>

        <?php }?>

    </section>
</section>

<?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl);
?>

<?php }
}
