<?php
/* Smarty version 3.1.30, created on 2017-06-05 17:38:48
  from "/Users/jasenpeterson/Sites/haven/wp-content/themes/haven/smarty_templates/pages/interior.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_593597283c4043_42150766',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'b17a145ac22a6c0f19e155d325082f65fa3ccbea' => 
    array (
      0 => '/Users/jasenpeterson/Sites/haven/wp-content/themes/haven/smarty_templates/pages/interior.tpl',
      1 => 1496684324,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:../global/pageBanner.tpl' => 1,
    'file:../global/teamProfiles.tpl' => 1,
  ),
),false)) {
function content_593597283c4043_42150766 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_subTemplateRender("file:../global/pageBanner.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('PageBannerImage'=>$_smarty_tpl->tpl_vars['PageBanner']->value['url'],'PageSlug'=>$_smarty_tpl->tpl_vars['pageSlug']->value), 0, false);
?>




<?php if ($_smarty_tpl->tpl_vars['ShowSideBar']->value == 'true') {?>
  <section class="InteriorPage <?php echo $_smarty_tpl->tpl_vars['pageSlug']->value;
if (empty($_smarty_tpl->tpl_vars['PageBanner']->value['url'])) {?> InteriorPagePaddingTop <?php }?>">
    <section class="container">
      <section class="grid">
        <section class="row">
          <section class="gr-4 SideBar">
            <section class="SideBarContainer">
              <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 827 709" class="Slurve">
                  <path d="M-1 51.8L827 0v709.1H-1V51.8z"/>
              </svg>
              <svg xmlns="http://www.w3.org/2000/svg" class="SideBarTop" viewBox="0 0 285 39">
                <path d="M0 23L285 0v39H0"/>
              </svg>
              <?php ob_start();
echo $_smarty_tpl->tpl_vars['pageID']->value;
$_prefixVariable1=ob_get_clean();
echo $_smarty_tpl->smarty->registered_objects['SideBar'][0]->displaySideBar(array('pageID'=>$_prefixVariable1),$_smarty_tpl);?>

            </section>
          </section>
          <section class="gr-8 gr-12@xs gr-12@sm">
            <section class="box">
              <section class="Title">
                  <svg class="Diamond" width="32px" height="32px" viewBox="0 0 32 32" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                      <polygon points="16,2 30,16 16,30 2,16"></polygon>
                  </svg>
                  <h1><?php echo $_smarty_tpl->tpl_vars['Title']->value;?>
</h1>
              </section>
              <?php echo $_smarty_tpl->tpl_vars['Content']->value;?>

            </section>
          </section>
        </section>
      </section>
    </section>
  </seection>
<?php } else { ?>
  <section class="InteriorPage <?php echo $_smarty_tpl->tpl_vars['pageSlug']->value;?>
">
    <section class="container">
      <section class="grid">
        <section class="row row-align-center">
          <section class="gr-10 gr-12@xs gr-12@sm">
            <section class="box">
              <section class="Title">
                  <svg class="Diamond" width="32px" height="32px" viewBox="0 0 32 32" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                      <polygon points="16,2 30,16 16,30 2,16"></polygon>
                  </svg>
                  <h1><?php echo $_smarty_tpl->tpl_vars['Title']->value;?>
</h1>
              </section>
              <?php echo $_smarty_tpl->tpl_vars['Content']->value;?>

            </section>
            <?php $_smarty_tpl->_subTemplateRender("file:../global/teamProfiles.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('TeamProfiles'=>$_smarty_tpl->tpl_vars['TeamProfiles']->value), 0, false);
?>

          </section>
        </section>
      </section>
    </section>
  </section>
<?php }
}
}
