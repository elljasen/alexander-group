<?php
/* Smarty version 3.1.30, created on 2017-07-13 22:23:19
  from "/Users/jasenpeterson/Sites/tag/wp-content/themes/TAG/smarty_templates/global/recentPost.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_5967f2d7e96419_56382688',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '01055bcff2d0a59286989f987d7e5a1e2fa0be54' => 
    array (
      0 => '/Users/jasenpeterson/Sites/tag/wp-content/themes/TAG/smarty_templates/global/recentPost.tpl',
      1 => 1499984596,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5967f2d7e96419_56382688 (Smarty_Internal_Template $_smarty_tpl) {
?>
<section class="recentPost">
  <section class="box">
    <section class="recentPostTitle">
      <h1>Recent <span>Articles</span></h1>
    </section>
    <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['RecentPost']->value['post'], 'Post');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['Post']->value) {
?>
      <?php if ($_smarty_tpl->tpl_vars['Post']->value['PostID'] !== $_smarty_tpl->tpl_vars['pageID']->value) {?>
        <article>
          <a href="<?php echo $_smarty_tpl->tpl_vars['Post']->value['URL'];?>
">
            <h1><?php echo $_smarty_tpl->tpl_vars['Post']->value['Title'];?>
</h1>
          </a>
        </article>
      <?php }?>
    <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl);
?>

  </section>
</section>
<?php }
}
