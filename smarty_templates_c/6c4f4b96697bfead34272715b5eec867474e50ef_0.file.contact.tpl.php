<?php
/* Smarty version 3.1.30, created on 2017-06-05 18:07:56
  from "/Users/jasenpeterson/Sites/haven/wp-content/themes/haven/smarty_templates/pages/contact.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_59359dfcd7e187_88424907',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '6c4f4b96697bfead34272715b5eec867474e50ef' => 
    array (
      0 => '/Users/jasenpeterson/Sites/haven/wp-content/themes/haven/smarty_templates/pages/contact.tpl',
      1 => 1496686073,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:../global/pageBanner.tpl' => 1,
  ),
),false)) {
function content_59359dfcd7e187_88424907 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_subTemplateRender("file:../global/pageBanner.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('PageBannerImage'=>$_smarty_tpl->tpl_vars['PageBanner']->value['url'],'PageSlug'=>$_smarty_tpl->tpl_vars['pageSlug']->value), 0, false);
?>

<section class="GoogleMap">
  <div class="mapCanvas acf-map">
    <div class="marker" data-lat="<?php echo $_smarty_tpl->tpl_vars['Map']->value['lat'];?>
" data-lng="<?php echo $_smarty_tpl->tpl_vars['Map']->value['lng'];?>
"></div>
  </div>
</section>

<section class="InteriorPage <?php echo $_smarty_tpl->tpl_vars['pageSlug']->value;?>
 Contact">
  <section class="container">
    <section class="grid">
      <section class="row row-align-center">
        <section class="gr-10 gr-12@sm gr-12@xs">
          <section class="box">
            <section class="Title">
                <svg class="Diamond" width="32px" height="32px" viewBox="0 0 32 32" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                    <polygon points="16,2 30,16 16,30 2,16"></polygon>
                </svg>
                <h1><?php echo $_smarty_tpl->tpl_vars['Title']->value;?>
</h1>
            </section>
            <?php echo $_smarty_tpl->tpl_vars['Content']->value;?>

            <section class="ContactForm">
            <?php echo $_smarty_tpl->tpl_vars['ContactForm']->value;?>

            </section>
          </section>
        </section>
      </section>
    </section>
  </section>
</section>
<?php }
}
