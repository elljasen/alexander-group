<?php
/* Smarty version 3.1.30, created on 2017-08-09 15:09:51
  from "/Users/jasenpeterson/Sites/tag/wp-content/themes/TAG/smarty_templates/pages/locations.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_598b25bf038f89_52500691',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'ea38f748d5332e4d6a9dc9584c7fcb9100703ae6' => 
    array (
      0 => '/Users/jasenpeterson/Sites/tag/wp-content/themes/TAG/smarty_templates/pages/locations.tpl',
      1 => 1502291386,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:../global/pageBanner.tpl' => 1,
  ),
),false)) {
function content_598b25bf038f89_52500691 (Smarty_Internal_Template $_smarty_tpl) {
if (!is_callable('smarty_modifier_replace')) require_once '/Users/jasenpeterson/Sites/tag/wp-content/themes/TAG/smarty/libs/plugins/modifier.replace.php';
$_smarty_tpl->_subTemplateRender("file:../global/pageBanner.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('Banner'=>$_smarty_tpl->tpl_vars['LocationsBanner']->value,'BannerClass'=>'Locations Interior'), 0, false);
?>


<section class="FlexContainer Locations">

    <!-- ROW -->
    <section class="row RowTwo">

    <div class="column DarkGrayBackground"></div>

    <!-- COLUMN -->
    <section class="column LocationsList">
      <section class="box">
          <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['Locations']->value, 'location', false, NULL, 'locations', array (
  'iteration' => true,
));
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['location']->value) {
$_smarty_tpl->tpl_vars['__smarty_foreach_locations']->value['iteration']++;
?>
              <section class="LocationContainer <?php if ((isset($_smarty_tpl->tpl_vars['__smarty_foreach_locations']->value['iteration']) ? $_smarty_tpl->tpl_vars['__smarty_foreach_locations']->value['iteration'] : null) == 1) {?>activeLocation<?php }?>" data-location-id="<?php echo smarty_modifier_replace($_smarty_tpl->tpl_vars['location']->value['location'],' ','');?>
">
                  <h1>
                      <span><?php echo $_smarty_tpl->tpl_vars['location']->value['location'];?>
</span>
                      <span>
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 321.4 158.2">
    <path d="M313.6 71.8L245 3.2c-4.2-4.2-11-4.2-15.3 0-4.2 4.2-4.2 11 0 15.3l50.2 50.2H10.8C4.8 68.7 0 73.5 0 79.5s4.8 10.8 10.8 10.8h269.1l-50.2 50.2c-4.2 4.2-4.2 11 0 15.3 2.1 2.1 4.9 3.2 7.6 3.2 2.7 0 5.5-1 7.6-3.2l68.6-68.6c4.3-4.3 4.3-11.2.1-15.4zm0 0"/>
    </svg>
          </span>
                  </h1>
              </section>
          <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl);
?>

      </section>
    </section>
    <!-- /COLUMN -->

    <!-- COLUMN -->
    <section class="column">

      <section class="FlexContainer Horizontal">

        <!-- ROW -->
        <section class="row">

            <!-- COLUMN -->
            <section class="column Location">

              <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['Locations']->value, 'Addresses', false, NULL, 'addresses', array (
  'iteration' => true,
));
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['Addresses']->value) {
$_smarty_tpl->tpl_vars['__smarty_foreach_addresses']->value['iteration']++;
?>

                <section class="box LocationBox FlexContainer Horizontal <?php if ((isset($_smarty_tpl->tpl_vars['__smarty_foreach_addresses']->value['iteration']) ? $_smarty_tpl->tpl_vars['__smarty_foreach_addresses']->value['iteration'] : null) == 1) {?>activeLocation<?php }?>" data-location-id="<?php echo smarty_modifier_replace($_smarty_tpl->tpl_vars['Addresses']->value['location'],' ','');?>
">

                  <!-- ROW -->
                  <section class="row">

                    <!-- MAP -->
                    <section class="Map column">
                        <section class="box">
                            <section class="mapCanvas Circular acf-map">
                              <div class="marker" data-lat="<?php echo $_smarty_tpl->tpl_vars['Addresses']->value['map']['lat'];?>
" data-lng="<?php echo $_smarty_tpl->tpl_vars['Addresses']->value['map']['lng'];?>
"></div>
                            </section>
                            <section class="Address">
                                <section class="AddressContainer">
                                    <h2><?php echo $_smarty_tpl->tpl_vars['Addresses']->value['addresses'][0]['street'];?>
</h2>
                                    <h1><?php echo $_smarty_tpl->tpl_vars['Addresses']->value['addresses'][0]['location'];?>
</h1>
                                </section>

                                <!-- PHONE -->
                                <section class="Phone">
                                    <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['Addresses']->value['addresses'][0]['phone'], 'Phone', false, NULL, 'phone', array (
));
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['Phone']->value) {
?>
                                        <h1><?php echo $_smarty_tpl->tpl_vars['Phone']->value['number'];?>
</h1>
                                    <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl);
?>

                                </section>
                                <!-- /PHONE -->
                            </section>
                        </section>
                    </section>
                    <!-- /MAP -->

                  </section>
                  <!--/ROW -->

                </section>

              <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl);
?>


            </section>
            <!-- /COLUMN -->

        </section>
        <!-- /ROW -->

      </section>

    </section>
    <!-- /COLUMN -->

    <div class="column LightBlueBackgroundBlock"></div>

    </section>
    <!-- /ROW -->

    <section class="row contactForm">
        <section class="column">

        </section>
        <section class="column">
            <?php echo $_smarty_tpl->tpl_vars['LocationsForm']->value;?>

        </section>
    </section>
    <!-- /ROW -->

</section>
<?php }
}
