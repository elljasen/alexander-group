<?php
/* Smarty version 3.1.30, created on 2017-06-13 16:17:10
  from "/Users/jasenpeterson/Sites/tag/wp-content/themes/TAG/smarty_templates/global/NewsList.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_5940100626d5b7_15877836',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '1ec1160ec3a8bb6033beeb6f3e7523dd2f295fdc' => 
    array (
      0 => '/Users/jasenpeterson/Sites/tag/wp-content/themes/TAG/smarty_templates/global/NewsList.tpl',
      1 => 1497370628,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5940100626d5b7_15877836 (Smarty_Internal_Template $_smarty_tpl) {
?>
<section class="News">
  <section class="box">

    <section class="NewsTitle"><h1>Tag in the <span>News</span></h1></section>

    <section class="Articles">
      <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['NewsList']->value['post'], 'News');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['News']->value) {
?>
       <article>
         <a href="<?php echo $_smarty_tpl->tpl_vars['News']->value['URL'];?>
" class="ReadMore">
           <h1><?php echo $_smarty_tpl->tpl_vars['News']->value['Title'];?>
</h1>
           <span>Read More</span>
         </a>
       </article>
      <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl);
?>

    </section>

  </section>
  <a class="btn" href="<?php echo $_smarty_tpl->tpl_vars['homeURL']->value;?>
/<?php echo $_smarty_tpl->tpl_vars['NewsPostSlug']->value;?>
">
    <span>Read More News</span>
    <div class="line"></div>
  </a>
</section>
<?php }
}
