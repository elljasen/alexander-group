<?php
/* Smarty version 3.1.30, created on 2017-08-10 21:34:55
  from "/Users/jasenpeterson/Sites/tag/wp-content/themes/TAG/smarty_templates/global/cards.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_598cd17f9df579_19815052',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'b17ca980ef052fce0dd125635702409e4238a3e9' => 
    array (
      0 => '/Users/jasenpeterson/Sites/tag/wp-content/themes/TAG/smarty_templates/global/cards.tpl',
      1 => 1502400890,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:../global/leadershipProfile.tpl' => 1,
  ),
),false)) {
function content_598cd17f9df579_19815052 (Smarty_Internal_Template $_smarty_tpl) {
?>
<section class="Cards <?php echo $_smarty_tpl->tpl_vars['CardClass']->value;?>
" style="position: relative; height: 100vh;">
  <section class="FlexContainer Horizontal" style="position: absolute; left: 0; top: 0; height: 100vh;">
    <div class="column DarkBlueBackgroundBlock"></div>
      <section class="column">
          <section class="FlexContainer Horizontal CardsContainer">
            <section class="row">
            <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['TeamProfiles']->value, 'TeamProfile', false, NULL, 'CardLoop', array (
  'iteration' => true,
));
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['TeamProfile']->value) {
$_smarty_tpl->tpl_vars['__smarty_foreach_CardLoop']->value['iteration']++;
?>
              <section class="column <?php if (!(1 & (isset($_smarty_tpl->tpl_vars['__smarty_foreach_CardLoop']->value['iteration']) ? $_smarty_tpl->tpl_vars['__smarty_foreach_CardLoop']->value['iteration'] : null))) {?>EvenCard<?php } else { ?>OddCard<?php }?>">
                <section class="CardContainer">
                  <section class="Image">
                    <img src="<?php echo $_smarty_tpl->tpl_vars['TeamProfile']->value['image']['sizes']['medium_large'];?>
" alt="$TeamProfile.image.alt">
                  </section>
                  <section class="box ContentModule">
                    <h1><?php echo $_smarty_tpl->tpl_vars['TeamProfile']->value['name'];?>
</h1>
                    <h2><?php echo $_smarty_tpl->tpl_vars['TeamProfile']->value['title'];?>
</h2>
                    <?php echo $_smarty_tpl->tpl_vars['TeamProfile']->value['description'];?>

                  </section>
                  <section class="FlexContainer Horizontal Buttons">
                    <section class="column"><a class="btn odd ProfileButton AjaxButton" data-id="<?php echo $_smarty_tpl->tpl_vars['TeamProfile']->value['full_profile']->ID;?>
" href="javascript:void(0);">Read More</a></section>
                    <section class="column"><a class="btn even" href="javascript:void(0);">Download VCard</a></section>
                  </section>
                </section>
              </section>
              <?php if (!(1 & (isset($_smarty_tpl->tpl_vars['__smarty_foreach_CardLoop']->value['iteration']) ? $_smarty_tpl->tpl_vars['__smarty_foreach_CardLoop']->value['iteration'] : null))) {?></section><section class="row"><?php }?>
            <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl);
?>

          </section>
      </section>
  </section>
</section>

<?php $_smarty_tpl->_subTemplateRender("file:../global/leadershipProfile.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

<?php }
}
