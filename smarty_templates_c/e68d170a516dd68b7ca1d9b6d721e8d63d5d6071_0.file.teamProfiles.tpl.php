<?php
/* Smarty version 3.1.30, created on 2017-06-05 17:58:45
  from "/Users/jasenpeterson/Sites/haven/wp-content/themes/haven/smarty_templates/global/teamProfiles.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_59359bd5377f83_88378205',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'e68d170a516dd68b7ca1d9b6d721e8d63d5d6071' => 
    array (
      0 => '/Users/jasenpeterson/Sites/haven/wp-content/themes/haven/smarty_templates/global/teamProfiles.tpl',
      1 => 1496685522,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_59359bd5377f83_88378205 (Smarty_Internal_Template $_smarty_tpl) {
if (!empty($_smarty_tpl->tpl_vars['TeamProfiles']->value)) {?>
  <section class="TeamProfiles">
    <section class="row TeamProfilesTitle">
      <section class="gr-12">
        <h1><?php echo $_smarty_tpl->tpl_vars['TeamProfilesTitle']->value;?>
</h1>
      </section>
    </section>
    <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['TeamProfiles']->value, 'Team');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['Team']->value) {
?>
      <section class="row row-align-middle">
        <section class="gr-3 gr-12@xs gr-12@sm Image">
          <span>
            <img src="<?php echo $_smarty_tpl->tpl_vars['Team']->value['image']['url'];?>
" alt="<?php echo $_smarty_tpl->tpl_vars['Team']->value['image']['alt'];?>
">
          </span>
        </section>
        <section class="gr-9 gr-12@xs gr-12@sm Profiles">
          <section class="box">
            <section class="grid">
              <section class="row row-align-middle">
                <section class="gr-6 gr-12@xs gr-12@sm Bio">
                  <h1><?php echo $_smarty_tpl->tpl_vars['Team']->value['name'];?>
</h1>
                  <h2><?php echo $_smarty_tpl->tpl_vars['Team']->value['title'];?>
</h2>
                  <?php echo $_smarty_tpl->tpl_vars['Team']->value['content'];?>

                </section>
                <section class="gr-6 gr-12@xs gr-12@sm Education">
                  <?php echo $_smarty_tpl->tpl_vars['Team']->value['education'];?>

                </section>
              </section>
            </section>
          </section>
        </section>
      </section>
    <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl);
?>

  </section>
<?php }
}
}
