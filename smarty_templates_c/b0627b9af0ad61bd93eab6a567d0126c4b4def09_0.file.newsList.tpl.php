<?php
/* Smarty version 3.1.30, created on 2017-07-14 14:36:06
  from "/Users/jasenpeterson/Sites/tag/wp-content/themes/TAG/smarty_templates/global/newsList.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_5968d6d6018431_07666591',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'b0627b9af0ad61bd93eab6a567d0126c4b4def09' => 
    array (
      0 => '/Users/jasenpeterson/Sites/tag/wp-content/themes/TAG/smarty_templates/global/newsList.tpl',
      1 => 1500042962,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5968d6d6018431_07666591 (Smarty_Internal_Template $_smarty_tpl) {
?>
<section class="News">
  <section class="box">

    <section class="NewsTitle"><h1>Blog</h1></section>

    <section class="Articles">
      <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['NewsList']->value['post'], 'News');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['News']->value) {
?>
       <article>
         <a href="<?php echo $_smarty_tpl->tpl_vars['News']->value['URL'];?>
" class="ReadMore">
           <h1><?php echo $_smarty_tpl->tpl_vars['News']->value['Excerpt'];?>
</h1>
           <span>Read More</span>
         </a>
       </article>
      <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl);
?>

    </section>

  </section>
  <a class="btn" href="<?php echo $_smarty_tpl->tpl_vars['homeURL']->value;?>
/<?php echo $_smarty_tpl->tpl_vars['NewsPostSlug']->value;?>
">
    <span>Read More</span>
    <div class="line"></div>
  </a>
</section>
<?php }
}
