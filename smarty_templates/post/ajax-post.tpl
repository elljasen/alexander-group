<article>
    <a href="{$postURL}" class="FlexContainer Horizontal">

        <section class="image column BackgroundImage" style="background: url({$postImage[0]})"></section>

        <section class="PostContent column">
            <section class="box">

                <h1 class="title">{$postTitle}</h1>

                <section class="PostMeta">
                    <span class="PostAuthor">by: {$postAuthor} </span>
                    <span class="PostDate">{$postDate}</span>
                </section>

            </section>
        </section>

    </a>
</article>