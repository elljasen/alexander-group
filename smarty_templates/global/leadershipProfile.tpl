<section class="AjaxLoader">
  <img src="{$themeURL}/assets/images/icons/rolling.svg" alt="">
  <section class="TeamProfileFull AjaxContainer">
    <section class="CloseButton">
      <svg xmlns="http://www.w3.org/2000/svg" id="Layer_1" viewBox="0 0 640 480" class="AjaxCloseButton">
        <path d="M319.4 126C256.2 126 205 177.2 205 240.4s51.2 114.4 114.4 114.4 114.4-51.2 114.4-114.4S382.6 126 319.4 126zm0 207.4c-51.3 0-93-41.6-93-93 0-51.3 41.6-93 93-93s93 41.6 93 93-41.6 93-93 93zm20.3-93l20.2-20.2c2.8-2.8 2.8-7.3 0-10.1L349.8 200c-2.8-2.8-7.3-2.8-10.1 0l-20.2 20.2-20.2-20.2c-2.8-2.8-7.3-2.8-10.1 0L279 210c-2.8 2.8-2.8 7.3 0 10.1l20.2 20.2-20.2 20.2c-2.8 2.8-2.8 7.3 0 10.1l10.1 10.1c2.8 2.8 7.3 2.8 10.1 0l20.2-20.2 20.3 20.3c2.8 2.8 7.3 2.8 10.1 0l10.1-10.1c2.8-2.8 2.8-7.3 0-10.1l-20.2-20.2zm0 0" class="st0"/>
      </svg>
    </section>
    <section class="FlexContainer">
      <section class="column Name">
        <section class="box">
          <h1></h1>
        </section>
      </section>
      <section class="FlexContainer Horizontal ColumnVertical">
        <section class="column Image"></section>
        <section class="column Content">
          <h1></h1>
          <section class="Description"></section>
          <a class="btn" href="">
            <span>Learn More</span>
            <div class="line"></div>
          </a>
        </section>
      </section>
      <section class="FlexContainer Experience">
        <section class="column ExperienceLabel">
          <section class="box">
            <h1></h1>
          </section>
        </section>
        <section class="column">
          <section class="FlexContainer Horizontal">
            <ul class="ExperienceList">

            </ul>
          </section>
        </section>
      </section>
    </section>
  </section>
</section>
