<section class="blogLayout blogLayoutParent FlexContainer Horizontal">

  <div class="column WhiteBackgroundBlock"></div>

  <section class="column blogContent">
    <section class="box">

        <!--articlesToggle-->
        <section class="articlesToggle FlexContainer Horizontal">
        {*<a class="btn BlogButton active" href="javascript:void(0)" data-name="post">*}
          {*<span>Tag Articles</span>*}
          {*<div class="line"></div>*}
        {*</a>*}
        {*<a class="btn BlogButton" href="javascript:void(0)" data-name="tag_news">*}
          {*<span>Tag in the News</span>*}
          {*<div class="line"></div>*}
        {*</a>*}
        </section>
        <!--/articlesToggle-->

        <!-- PostContainer -->
        <section class="PostContainerWrap active post">
            <section class="PostContainer">

                <section class="FeaturedPostContainer FlexContainer Horizontal ColumnVertical">

                    {foreach $BlogPost['post'] as $Post name=blogpost}
                        {if $smarty.foreach.blogpost.iteration <= 2 }
                            <article class="column">
                                <a href="{$Post['URL']}">

                                    <section class="image BackgroundImage" style="background: url({$Post['Image']})"></section>

                                    <section class="PostContent">

                                        <h1 class="title">{$Post['Title']}</h1>

                                        <section class="PostMeta">
                                            <span class="PostAuthor">by: {$Post['Author']} </span>
                                            <span class="PostDate">{$Post['Date']}</span>
                                        </section>

                                    </section>

                                </a>
                            </article>
                        {/if}
                    {/foreach}
                </section>
                <!--/FeaturedPostContainer-->

                <section class="PostContainer PostList FlexContainer Horizontal ColumnVertical">
                    <section class="column">
                        <nav data-post-type="post" class="navigation pagination">{$Pagination}</nav>
                        <section class="AjaxLoader" style="opacity: 0; background: none; display: none; height: 100%;
                        position: absolute; top: 0; left: 0; align-items: flex-start">
                            <img class="ajax-loader" src="{$themeURL}/assets/images/icons/rolling.svg" alt="" style="margin: 10rem auto;">
                        </section>
                        <section class="articleList" style="position: relative;">
                            {foreach $BlogPost['post'] as $Post name=blogpost}
                                {if $smarty.foreach.blogpost.iteration > 2 }
                                    <article>
                                        <a href="{$Post['URL']}" class="FlexContainer Horizontal">

                                            <section class="image column BackgroundImage" style="background: url({$Post['Image']})"></section>

                                            <section class="PostContent column">
                                                <section class="box">

                                                    <h1 class="title">{$Post['Title']}</h1>

                                                    <section class="PostMeta">
                                                        <span class="PostAuthor">by: {$Post['Author']} </span>
                                                        <span class="PostDate">{$Post['Date']}</span>
                                                    </section>

                                                </section>
                                            </section>

                                        </a>
                                    </article>
                                {/if}
                            {/foreach}
                        </section>
                    </section>
                    <section class="column TwitterContainer">
                        {include file='../global/twitterFeed.tpl' TwitterFeed=$TwitterFeed}
                    </section>
                </section>

            </section>
            <!-- /PostContainer -->

        </section>

        <section class="PostContainerWrap tag_news">
            <!-- PostContainer -->
            <section class="PostContainer">

                <section class="FeaturedPostContainer FlexContainer Horizontal ColumnVertical">
                    {foreach $BlogPost['tag_news'] as $Post name=blogpost}
                        {if $smarty.foreach.blogpost.iteration <= 2 }
                            <article class="column">
                                <a href="{$Post['URL']}">

                                    <section class="image BackgroundImage" style="background: url({$Post['Image']})"></section>

                                    <section class="PostContent">

                                        <h1 class="title">{$Post['Title']}</h1>

                                        <section class="PostMeta">
                                            <span class="PostAuthor">by: {$Post['Author']} </span>
                                            <span class="PostDate">{$Post['Date']}</span>
                                        </section>

                                    </section>

                                </a>
                            </article>
                        {/if}
                    {/foreach}

                </section>
                <!--/FeaturedPostContainer-->

                <section class="PostContainer PostList FlexContainer Horizontal ColumnVertical">
                    <section class="column articleList">

                        {foreach $BlogPost['post'] as $Post name=blogpost}
                            {if $smarty.foreach.blogpost.iteration > 2 }
                                <article>
                                    <a href="{$Post['URL']}" class="FlexContainer Horizontal">

                                        <section class="image column BackgroundImage" style="background: url({$Post['Image']})"></section>

                                        <section class="PostContent column">
                                            <section class="box">

                                                <h1 class="title">{$Post['Title']}</h1>

                                                <section class="PostMeta">
                                                    <span class="PostAuthor">by: {$Post['Author']} </span>
                                                    <span class="PostDate">{$Post['Date']}</span>
                                                </section>

                                            </section>
                                        </section>

                                    </a>
                                </article>
                            {/if}
                        {/foreach}
                        <nav data-post-type="tag_news" class="paginationNav">{$Pagination}</nav>
                    </section>
                    <section class="column TwitterContainer">
                        {include file='../global/twitterFeed.tpl' TwitterFeed=$TwitterFeed}
                    </section>
                </section>

            </section>
            <!-- /PostContainer -->
        </section>

    </section>
  </section>

</section>
