<section class="News">
  <section class="box">

    <section class="NewsTitle"><h1>Blog</h1></section>

    <section class="Articles">
      {foreach from=$NewsList['post'] item=News}
       <article>
         <a href="{$News.URL}" class="ReadMore">
           <h1>{$News.Excerpt}</h1>
           <span>Read More</span>
         </a>
       </article>
      {/foreach}
    </section>

  </section>
  <a class="btn" href="{$homeURL}/{$NewsPostSlug}">
    <span>Read More</span>
    <div class="line"></div>
  </a>
</section>
