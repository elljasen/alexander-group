<section class="Cards {$CardClass}" style="position: relative; height: 100vh;">
  <section class="FlexContainer Horizontal" style="position: absolute; left: 0; top: 0; height: 100vh;">
    <div class="column DarkBlueBackgroundBlock"></div>
      <section class="column">
          <section class="FlexContainer Horizontal CardsContainer">
            <section class="row">
            {foreach from=$TeamProfiles item=TeamProfile name=CardLoop}
              <section class="column {if $smarty.foreach.CardLoop.iteration is even}EvenCard{else}OddCard{/if}">
                <section class="CardContainer">
                  <section class="Image">
                    <img src="{$TeamProfile.image.sizes.medium_large}" alt="$TeamProfile.image.alt">
                  </section>
                  <section class="box ContentModule">
                    <h1>{$TeamProfile.name}</h1>
                    <h2>{$TeamProfile.title}</h2>
                    {$TeamProfile.description}
                  </section>
                  <section class="FlexContainer Horizontal Buttons">
                    <section class="column"><a class="btn odd ProfileButton AjaxButton" data-id="{$TeamProfile.full_profile->ID}" href="javascript:void(0);">Read More</a></section>
                    <section class="column"><a class="btn even" href="javascript:void(0);">Download VCard</a></section>
                  </section>
                </section>
              </section>
              {if $smarty.foreach.CardLoop.iteration is even}</section><section class="row">{/if}
            {/foreach}
          </section>
      </section>
  </section>
</section>

{include file='../global/leadershipProfile.tpl'}
