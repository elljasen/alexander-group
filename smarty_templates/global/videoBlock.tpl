<section class="VideoBlock PageBanner {$Class}" style="background:url({$Video[0].poster.url})">
  <h1 class="Flex">
    <span>{$Video[0].title} &ndash;</span>
    <svg xmlns="http://www.w3.org/2000/svg" class="PlayButtonIcon" viewBox="0 0 410 411">
      <path d="M284.6 198.1l-108.1-73.9c-3.4-2.5-12.3-2.5-13 6.9v147.8c.8 9.4 9.9 9.5 13 6.9l108.1-73.9c2.8-1.7 6.6-8.6 0-13.8zm-104.4 65.1V146.8L265 205l-84.8 58.2zm0 0" class="st0"/>
      <path d="M206 9C97.9 9 10 96.9 10 205s87.9 196 196 196 196-87.9 196-196S314.1 9 206 9zm0 375.3c-98.9 0-179.3-80.4-179.3-179.3S107.1 25.7 206 25.7 385.3 106.1 385.3 205 304.9 384.3 206 384.3zm0 0" class="st0"/>
    </svg>
  </h1>
</section>
