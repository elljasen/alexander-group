{* /*{$PublicationsPostObject|@print_r}*/ *}
<section class="PublicationsFlexContainer FlexContainer">
  {foreach from=$Publications['publication'] item=Publication}
    <section class="column">
      <section class="box Publication">
        <a href="{$Publication.URL}">
          <section class="inner">
            <span class="PostDate">{$Publication.Date}</span>
            <h1>{$Publication.Title}</h1>
              {$Publication.Publications.Excerpt}
            <img src="{$Publication.Publications.Logo.url}" alt="{$Publication.Publications.Logo.alt}" />
          </section>
        </a>
      </section>
    </section>
  {/foreach}
</section>
