<section class="recentPost">
  <section class="box">
    <section class="recentPostTitle">
      <h1>Recent <span>Articles</span></h1>
    </section>
    {foreach $RecentPost['post'] as $Post}
      {if $Post['PostID'] !== $pageID}
        <article>
          <a href="{$Post['URL']}">
            <h1>{$Post['Title']}</h1>
          </a>
        </article>
      {/if}
    {/foreach}
  </section>
</section>
