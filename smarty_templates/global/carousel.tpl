<section class="Carousel">
    {foreach from=$Carousel item=CarouselItem}
        <section class="CarouselCell">
            <section class="container">
                <section class="grid">
                    <section class="row row-align-middle row-align-center">
                        <div class="gr-6 gr-12@xs gr-12@sm CarouselImage">
                            <section class="Inner">
                                <img src="{$CarouselItem.image.url}" alt="{$CarouselItem.image.alt}"/>
                            </section>
                        </div>
                        <!--/CaruselImage-->
                        <div class="gr-6 gr-12@xs gr-12@sm CarouselContent">
                            <section class="box">
                              <svg class="Diamond TwoDiamonds" width="32px" height="32px" viewBox="0 0 40 32" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                                  <polygon points="16,2 30,16 16,30 2,16"></polygon>
                                  <polygon points="16,2 30,16 16,30 2,16"></polygon>
                              </svg>
                              <h1>{$CarouselItem.title}</h1>
                              {$CarouselItem.content}
                              <a href="{$CarouselItem.button_link}" class="btn">
                                  <svg><rect x="0" y="0" fill="none" width="100%" height="100%"></rect></svg>
                                  Learn More
                              </a>
                            </section>
                        </div>
                        <!--/CarouselContent-->
                    </section>
                </section>
            </section>
        </section>
    {/foreach}
</section>
