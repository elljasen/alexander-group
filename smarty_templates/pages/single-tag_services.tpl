<section class="Services">
        <h1 class="servicesTitle">
            <span>{$ServiceName}</span>
        </h1>
        <section class="serviceDescription">
            <section class="inner">
                {$ServiceContent}
            </section>
        </section>
    </section>
</section>
