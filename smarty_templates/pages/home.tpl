{assign var='NewsList' $NewPostObject}
{include file='../global/pageBanner.tpl' Banner=$PageBanner BannerClass='Home'}
{include file='../portals/aboutPortal.tpl' AboutPortal=$AboutPagePortal}
{include file='../portals/expertisePortal.tpl' ExpertisePortal=$ExpertisePagePortal}
{include file='../portals/locationsPortal.tpl' LocationsPortal=$LocationsPagePortal}
{include file='../global/publications.tpl' PublicationsPostObject=$Publications}
