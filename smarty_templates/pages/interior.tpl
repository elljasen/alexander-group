{include file='../global/pageBanner.tpl' PageBannerImage=$PageBanner.url PageSlug=$pageSlug}

{* different page layouts for interior page - sidebar/no sidebar*}

{if $ShowSideBar == 'true'}
  <section class="InteriorPage {$pageSlug}{if empty($PageBanner.url)} InteriorPagePaddingTop {/if}">
    <section class="container">
      <section class="grid">
        <section class="row">
          <section class="gr-4 SideBar">
            <section class="SideBarContainer">
              <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 827 709" class="Slurve">
                  <path d="M-1 51.8L827 0v709.1H-1V51.8z"/>
              </svg>
              <svg xmlns="http://www.w3.org/2000/svg" class="SideBarTop" viewBox="0 0 285 39">
                <path d="M0 23L285 0v39H0"/>
              </svg>
              {SideBar->displaySideBar pageID={$pageID}}
            </section>
          </section>
          <section class="gr-8 gr-12@xs gr-12@sm">
            <section class="box">
              <section class="Title">
                  <svg class="Diamond" width="32px" height="32px" viewBox="0 0 32 32" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                      <polygon points="16,2 30,16 16,30 2,16"></polygon>
                  </svg>
                  <h1>{$Title}</h1>
              </section>
              {$Content}
            </section>
          </section>
        </section>
      </section>
    </section>
  </seection>
{else}
  <section class="InteriorPage {$pageSlug}">
    <section class="container">
      <section class="grid">
        <section class="row row-align-center">
          <section class="gr-10 gr-12@xs gr-12@sm">
            <section class="box">
              <section class="Title">
                  <svg class="Diamond" width="32px" height="32px" viewBox="0 0 32 32" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                      <polygon points="16,2 30,16 16,30 2,16"></polygon>
                  </svg>
                  <h1>{$Title}</h1>
              </section>
              {$Content}
            </section>
            {include file='../global/teamProfiles.tpl' TeamProfiles=$TeamProfiles}
          </section>
        </section>
      </section>
    </section>
  </section>
{/if}
