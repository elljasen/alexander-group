{include file='../global/pageBanner.tpl' Banner=$ServicesPageBanner BannerClass='Services Interior'}

<section class="Services Horizontal FlexContainer">

    <!-- ROW -->
    <section class="row RowOne">

      <div class="column LightBlueBackgroundBlock"></div>

      <section class="column">
        <section class="box">
          <h1>{$ServicesContent[0]['row'][0]['label']} &ndash;</h1>
          {$ServicesContent[0]['row'][0]['content']}
        </section>
      </section>

      <div class="column DarkGrayBackground"></div>

    </section>
    <!-- /ROW -->

    <!-- ROW -->
    <section class="row RowTwo">

      <section class="column">
        <section class="BackgroundImage" style="background: url({$ServicesContent[0]['row'][1]['image']['url']})"></section>
        <section class="box">
          {$ServicesContent[0]['row'][2]['content']}
        </section>
      </section>

      <section class="column">
        <section class="box service-content">
          {$ServicesContent[0]['row'][1]['content']}
        </section>
        <section class="services-list">
          <section class="box">
            <h1>Industry &ndash;</h1>
            {foreach $IndustryServices as $service}
            <a href="javascript:void(0)" class="btn AjaxButton" data-id="{$service.description}">
              <span>{$service.name}</span>
              <div class="line"></div>
            </a>
            {/foreach}
            <h1>Function &ndash;</h1>
            {foreach $FunctionServices as $service}
            <a href="javascript:void(0)" class="btn AjaxButton" data-id="{$service.description}">
              <span>{$service.name}</span>
              <div class="line"></div>
            </a>
            {/foreach}
          </section>
          <section class="BackgroundImage" style="background: url({$ServicesContent[0]['row'][2]['image']['url']})"></section>
        </section>
      </section>

    </section>
    <!-- /ROW -->

</section>

{* services description template *}

{include file='../global/servicesDescription.tpl'}
