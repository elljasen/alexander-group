{include file='../global/pageBanner.tpl' PageBannerImage=$PageBanner.url PageSlug=$pageSlug}
<section class="GoogleMap">
  <div class="mapCanvas acf-map">
    <div class="marker" data-lat="{$Map.lat}" data-lng="{$Map.lng}"></div>
  </div>
</section>

<section class="InteriorPage {$pageSlug} Contact">
  <section class="container">
    <section class="grid">
      <section class="row row-align-center">
        <section class="gr-10 gr-12@sm gr-12@xs">
          <section class="box">
            <section class="Title">
                <svg class="Diamond" width="32px" height="32px" viewBox="0 0 32 32" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                    <polygon points="16,2 30,16 16,30 2,16"></polygon>
                </svg>
                <h1>{$Title}</h1>
            </section>
            {$Content}
            <section class="ContactForm">
            {$ContactForm}
            </section>
          </section>
        </section>
      </section>
    </section>
  </section>
</section>
