<section class="blogLayout blogLayoutChild FlexContainer Horizontal">

  <div class="column WhiteBackgroundBlock"></div>

  <section class="column blogContent">
    <section class="box">
      <section class="blogBreadcrumb">
          {the_breadcrumb()}
      </section>
      <section class="PostContainer">

        <section class="BackgroundImage" style="background: url({$SinglePostImage})">
          {*$SinglePost|@print_r*}
          <section class="SinglePostTitle">
            <section class="box">

                <h1>{$SinglePost->post_title}</h1>

                <section class="PostMeta">
                  <span class="PostAuthor">by: {$SinglePostAuthor} </span>
                  <span class="PostDate">{$SinglePostDate}</span>
                </section>

            </section>
          </section>
        </section>

        <section class="FlexContainer Horizontal ColumnVertical SinglePostContainer">
          <section class="column SinglePostContent">
            {$SinglePostContent}
          </section>
          <section class="column RecentPost">
            {include file='../global/recentPost.tpl' RecentPost=$RecentPost}
          </section>
        </section>

      </section>
    </section>
  </section>

</section>
