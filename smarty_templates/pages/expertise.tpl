{include file='../global/pageBanner.tpl' Banner=$ExpertiseBanner BannerClass='Expertise Interior'}

<section class="ExpertiseBannerContent">
  <section class="box">
    {$ExpertiseBannerContent}
  </section>
</section>

<section class="ExpertiseIntro Horizontal FlexContainer">

  <!-- COLUMN -->
  <section class="column">

    <!--ROW-->
    <section class="row RowOne">

      <!--COLUMN-->
      <section class="column HeroText">
        <section class="box">
          {$ExpertiseIntro[0]['column_one'][0].content}
        </section>
      </section>
      <!--/COLUMN-->

      <!--COLUMN-->
      <section class="column">
        <section class="box">
          <h1>{$ExpertiseIntro[0]['column_two'][0].label} &ndash;</h1>
          {$ExpertiseIntro[0]['column_two'][0].content}
        </section>
      </section>
      <!--/COLUMN-->

    </section>
    <!--/ROW-->

    <!--ROW-->
    <section class="row RowTwo">

      <!--COLUMN-->
      <section class="column">
        <section class="box">
          <h1>{$ExpertiseIntro[1]['column_one'][0].label} &ndash;</h1>
          {$ExpertiseIntro[1]['column_one'][0].content}
        </section>
      </section>
      <!--/COLUMN-->

      <!--COLUMN-->
      <section class="column">
        <section class="box">
          {$ExpertiseIntro[1]['column_two'][0].content}
        </section>
        <section class="BackgroundImage" style="background: url({$ExpertiseIntro[1]['column_two'][0].image.url})"></section>
      </section>
      <!--/COLUMN-->

    </section>
    <!--/ROW-->

  </section>
  <!--/COLUMN-->

  <div class="column LightBlueBackgroundBlock"></div>

</section>

<section class="ExpertisePageBreak FlexContainer" style="background: url({$ExpertisePageBreakImage['url']})">
  <section class="row">
    {foreach $ExpertisePageBreak[0]['column'] as $PageBreak}
    <section class="column">
      <h1>{$PageBreak.Stat}</h1>
      <h2>{$PageBreak.label}</h2>
      {$PageBreak.content}
    </section>
    {/foreach}
  </section>
</section>
