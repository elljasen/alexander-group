{include file='../global/pageBanner.tpl' Banner=$LocationsBanner BannerClass='Locations Interior'}

<section class="FlexContainer Locations">

    <!-- ROW -->
    <section class="row RowTwo">

    <div class="column DarkGrayBackground"></div>

    <!-- COLUMN -->
    <section class="column LocationsList">
      <section class="box">
          {foreach from=$Locations item=location name=locations}
              <section class="LocationContainer {if $smarty.foreach.locations.iteration == 1}activeLocation{/if}" data-location-id="{$location.location|replace:' ':''}">
                  <h1>
                      <span>{$location.location}</span>
                      <span>
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 321.4 158.2">
    <path d="M313.6 71.8L245 3.2c-4.2-4.2-11-4.2-15.3 0-4.2 4.2-4.2 11 0 15.3l50.2 50.2H10.8C4.8 68.7 0 73.5 0 79.5s4.8 10.8 10.8 10.8h269.1l-50.2 50.2c-4.2 4.2-4.2 11 0 15.3 2.1 2.1 4.9 3.2 7.6 3.2 2.7 0 5.5-1 7.6-3.2l68.6-68.6c4.3-4.3 4.3-11.2.1-15.4zm0 0"/>
    </svg>
          </span>
                  </h1>
              </section>
          {/foreach}
      </section>
    </section>
    <!-- /COLUMN -->

    <!-- COLUMN -->
    <section class="column">

      <section class="FlexContainer Horizontal">

        <!-- ROW -->
        <section class="row">

            <!-- COLUMN -->
            <section class="column Location">

              {foreach $Locations as $Addresses name=addresses}

                <section class="box LocationBox FlexContainer Horizontal {if $smarty.foreach.addresses.iteration == 1}activeLocation{/if}" data-location-id="{$Addresses.location|replace:' ':''}">

                  <!-- ROW -->
                  <section class="row">

                    <!-- MAP -->
                    <section class="Map column">
                        <section class="box">
                            <section class="mapCanvas Circular acf-map">
                              <div class="marker" data-lat="{$Addresses['map']['lat']}" data-lng="{$Addresses['map']['lng']}"></div>
                            </section>
                            <section class="Address">
                                <section class="AddressContainer">
                                    <h2>{$Addresses.addresses[0].street}</h2>
                                    <h1>{$Addresses.addresses[0].location}</h1>
                                </section>

                                <!-- PHONE -->
                                <section class="Phone">
                                    {foreach $Addresses.addresses[0]['phone'] as $Phone name=phone }
                                        <h1>{$Phone['number']}</h1>
                                    {/foreach}
                                </section>
                                <!-- /PHONE -->
                            </section>
                        </section>
                    </section>
                    <!-- /MAP -->

                  </section>
                  <!--/ROW -->

                </section>

              {/foreach}

            </section>
            <!-- /COLUMN -->

        </section>
        <!-- /ROW -->

      </section>

    </section>
    <!-- /COLUMN -->

    <div class="column LightBlueBackgroundBlock"></div>

    </section>
    <!-- /ROW -->

    <section class="row contactForm">
        <section class="column">

        </section>
        <section class="column">
            {$LocationsForm}
        </section>
    </section>
    <!-- /ROW -->

</section>
