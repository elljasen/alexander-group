<section class="TeamProfileFull">
    <section class="FlexContainer">
        <section class="column Name">
            <section class="box">
                <h1>{$ProfileName}</h1>
            </section>
        </section>
        <section class="FlexContainer Horizontal ColumnVertical">
            <section class="column Image">
                <img src="{$ProfileImage['url']}" alt="{$ProfileImage['alt']}">
            </section>
            <section class="column Content">
                <h1>{$ProfileLabel}</h1>
                <section class="Description">
                    {$ProfileContent}
                </section>
                <a class="btn" href="{$ProfileLink}">
                    <span>Learn More</span>
                    <div class="line"></div>
                </a>
            </section>
        </section>
        <section class="FlexContainer Experience">
            <section class="column ExperienceLabel">
                <section class="box">
                    <h1>{$ExperienceLabel}</h1>
                </section>
            </section>
            <section class="column">
                <section class="FlexContainer Horizontal">
                    <ul class="ExperienceList">
                        {foreach from=$ExperienceBullets item=bullet}
                            <li>
                                <h1>{$bullet['label']}</h1>
                                {$bullet['content']}
                            </li>
                        {/foreach}
                    </ul>
                </section>
            </section>
        </section>
    </section>
</section>