{include file='../global/pageBanner.tpl' Banner=$AboutPageBanner BannerClass='About Interior'}

<section class="AboutPageIntro FlexContainer">
  <div class="column LightBlueBackgroundBlock"></div>

  <section class="column">

    <section class="box Content">

      <section class="FlexContainer">

        <section class="column">
          <section class="box HeroText">
            <h1>{$AboutPageIntro[0].hero_text}</h1>
          </section>
        </section>

        <section class="column ContentModules">
          <section class="ContentModule box">
            <h1>{$AboutPageIntro[0].hero_content_title} &ndash;</h1>
            {$AboutPageIntro[0].hero_content}
          </section>
        </section>

      </section>

      <section class="FlexContainer Horizontal">
        <section class="column DarkGrayBackground">
          {foreach from=$AboutPageIntro[1].content_module item=Content}
            <section class="ContentModule box">
              <h1>{$Content.title} &ndash;</h1>
              {$Content.content}
            </section>
          {/foreach}
        </section>
        <section class="column BackgroundImage" style="background: url({$AboutPageIntro[1].image.url})"></section>
      </section>

    </section>

  </section>

  <div class="column DarkBlueBackgroundBlock"></div>
</section>

<section class="OurCulture">
  {include file='../global/videoBlock.tpl' Video=$Video Class="Interior"}
  <section class="OurCultureContent  min-height FlexContainer">

    <div class="column DarkBlueBackgroundBlock"></div>

    <section class="column ColumnOne min-height">

      <section class="FlexContainer Horizontal min-height">

        <div class="column DarkBlueBackgroundBlock min-height"></div>

        <section class="column DarkGrayBackground min-height OurCultureContentBox">
          <section class="ContentModule box">
            <h1 class="HeroText">{$OurCulture[0].hero_text}</h1>
            <h1>{$OurCulture[0].title} &ndash;</h1>
            {$OurCulture[0].content}
          </section>
        </section>

        <section class="column BabyBlueBackground min-height">

        </section>

      </section>

    </section>

    <section class="column ColumnTwo" style="min-height: 450px;">
      <section class="FlexContainer Horizontal" style="min-height: 450px;">
        <section class="column" style="min-height: 450px;">
          <section class="ContentModule box">
            <h1>{$OurCulture[1].title} &ndash;</h1>
            {$OurCulture[1].content}
          </section>
        </section>
      </section>
    </section>

  </section>
</section>
