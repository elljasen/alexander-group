{if !empty($AboutPortal)}

<section class="FlexContainer AboutPortalFlexContainer">

  <sectin class="row">

    <section class="column PagePortal">

      <section class="FlexContainer">

        <section class="row">
          <div class="column LightBlueBackgroundBlock"></div>
          <section class="column BackgroundImage" style="background: url({$AboutPortal.pagePortal[0].image.url}) no-repeat"></section>
          <section class="column Content">
            <section class="box PortalContent">
              <h1>{$AboutPortal.pagePortal[0].title} &ndash;</h1>
              {$AboutPortal.pagePortal[0].content}
              <a class="btn" href="{$homeURL}/{$AboutPortal.slug}">
                <span>Learn More</span>
                <div class="line"></div>
              </a>
            </section>
          </section>
          <section class="column CompanyMotto FullColumn">
            <section class="box">
              <h1 class="HeroText">{$CompanyMotto}</h1>
            </section>
          </section>
        </section>

      </section>

    </section>

    <section class="column NewsList">
      {include file='../global/newsList.tpl'}
    </section>

    <div class="column DarkBlueBackgroundBlock"></div>

  </section>
  <!--/.row-->

</section>
{/if}
