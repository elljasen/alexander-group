{if !empty($ExpertisePortal)}

<section class="FlexContainer ExpertisePortalFlexContainer PagePortal">

  <section class="row">
    <section class="BackgroundImage column" style="background: url('{$ExpertisePortal.pagePortal[0].image.url}') no-repeat;"></section>
    <section class="Content column">
      <section class="box PortalContent">
        <h1>{$ExpertisePortal.pagePortal[0].title} &ndash;</h1>
        {$ExpertisePortal.pagePortal[0].content}
        <a class="btn" href="{$homeURL}/{$ExpertisePortal.slug}">
          <span>Learn More</span>
          <div class="line"></div>
        </a>
      </section>
    </section>
  </section>

</section>

{/if}
