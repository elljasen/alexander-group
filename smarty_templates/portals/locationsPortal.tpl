{if !empty($LocationsPortal)}

<section class="FlexContainer LocationsPortalFlexContainer PagePortal">

  <section class="row">

    <section class="column Content">
      <section class="box PortalContent">
        <section class="inner">
          <h1>{$LocationsPortal.pagePortal[0].title} &ndash;</h1>
          {$LocationsPortal.pagePortal[0].content}
        </section>=
      </section>
      <div class="column LightBlueBackgroundBlock"></div>
      <section class="column LocationsList">
        <section class="box">
          {foreach from=$Locations item=location}
            <h1>{$location.location}</h1>
          {/foreach}
          <a class="btn btn-reverse" href="{$homeURL}/{$LocationsPortal.slug}">
            <span>View all our locations</span>
            <div class="line"></div>
          </a>
        </section>
      </section>
    </section>

    <section class="column BackgroundImage" style="background: url({$LocationsPortal.pagePortal[0].image.url}) no-repeat"></section>

  </section>

</section>

{/if}
