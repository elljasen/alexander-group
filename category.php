<?php
/**
 * Created by PhpStorm.
 * User: jasenpeterson
 * Date: 8/10/17
 * Time: 9:34 AM
 * Description: Category Page
 */

get_header();

get_template_part('templates/global/header');

get_template_part('loops/category', 'loop');

get_template_part('templates/global/footer');

get_footer();
