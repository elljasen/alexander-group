<?php

namespace SideBar;

use PostQuery\PostQuery;

class SideBar
{
    public function displaySideBar($pageID = null)
    {
      // Current Page ID
  		$currentPageID = $pageID['pageID'];

  		// Parent Page ID
  		$parentPageID = wp_get_post_parent_id( $currentPageID );

  		// List Child Pages

  		$args = array(

  			'child_of' => $parentPageID,
  			'title_li' => '',
  			'echo'     => false,
  			'sort_column' => 'menu_order'

  		);

  		$childrenPages = wp_list_pages( $args );

  		if ( $childrenPages ):

  			$output = '';

  			$output .= '<ul class="InteriorPageSidebar">';

  			$output .= $childrenPages;

  			$output .='</ul>';

  			echo $output;

  		endif;
    }
}
