<?php

namespace PagePortal;

class PagePortal
{
    public $PagePortalArray;
    private $PageIDs;

    public $CompanyMotto;
    public $Locations;

    public function __construct($PageID)
    {
        $this->PageIDs = $PageID;
    }

    public function displayPagePortal()
    {
        $args = array(

            'post_type' => 'page',
            'post__in' => $this->PageIDs,
            'orderby' => 'post__in'

        );


        $query = new \WP_Query($args);

        foreach($this->PageIDs as $PostID):

          if ($query->have_posts()) :

              while ($query->have_posts()) :

                  $query->the_post();

                  $CurrentPageSlug = get_post_field( 'post_name', $PostID );

                  $this->PagePortalArray[$CurrentPageSlug] = array (
                      'slug' => $CurrentPageSlug,
                      'pagePortal' => get_field('page_portal',$PostID)
                  );

                  // company motto :

                  if(get_field('company_motto',$PostID)):

                    $this->CompanyMotto = get_field('company_motto',$PostID);

                  endif;

                  // locations :

                  if(get_field('locations',$PostID)):

                    $this->Locations = get_field('locations',$PostID);

                  endif;

              endwhile;

          endif;

        endforeach;

    }
}
