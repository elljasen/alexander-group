<?php

namespace PostQuery;

class PostQuery
{
    public $CollectedPost;
    public $pagination;

    public function getPost($PostType, $PostCount)
    {

      // post query arguments :

        $paged = ( get_query_var( 'paged' ) ) ? absint( get_query_var( 'paged' ) ) : 1;

          $args = array(
              'post_type'      =>  $PostType,
              'status'         => 'published',
              'posts_per_page' => $PostCount,
              'paged' => $paged,
              'order' => 'DESC',
              'orderby' => 'date'
          );

        $query = new \WP_Query($args);

      // index counter :

      $CurrentIndex = 0;

      // query the post :

      if ($query -> have_posts()) : while ($query -> have_posts()) : $query -> the_post();

        $currentPostType = get_post_type();

          $big = 999999999; // need an unlikely integer

          $this->pagination = paginate_links( array(
              'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
              'current' => max( 1, get_query_var('paged') ),
              'total' => $query->max_num_pages,
              'prev_text'          => __( '', 'twentyfifteen' ),
              'next_text'          => __( '', 'twentyfifteen' ),
              'before_page_number' => '<span class="meta-nav screen-reader-text">' . __( 'Page', 'twentyfifteen' ) . ' </span>'
          ) );

          // featured img :

      $postAttachmentID = get_post_thumbnail_id(get_the_ID());
        $post_image = wp_get_attachment_image_src($postAttachmentID, 'full');
        $post_image_alt = get_post_meta($postAttachmentID, '_wp_attachment_image_alt', true);
        $postImage = ($post_image) ? $post_image[0] : '';
        $postAlt = ($post_image_alt) ? $post_image_alt : '';

      // collect post and store in associative array ie: array['post']['0']

      $this->CollectedPost[$currentPostType][$CurrentIndex++] = array(
        'Title' => get_the_title(),
        'Excerpt' => strip_tags(excerpt(50)),
        'Content' => get_the_content(),
        'URL' => get_permalink(get_the_ID()),
        'Image' => $postImage,
        'ImageAlt' => $postAlt,
        'Date' => get_the_date('M. j, Y'),
        'Author' => get_the_author(),
        'PostID' => get_the_ID(),
        'Publications' => ($PostType == 'publication') ? array( // acf fields for publications post type :
          'Excerpt' => get_field('excerpt'),
          'Content' => get_field('content'),
          'Logo' => get_field('company_logo')
          ) : ''
      );

      // print_r($this->CollectedPost);

      endwhile;
        endif;
    }
}
