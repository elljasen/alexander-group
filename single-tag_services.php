<?php
/**
 * Created by PhpStorm.
 * User: jasenpeterson
 * Date: 8/8/17
 * Time: 3:54 PM
 */

get_header();

get_template_part('templates/global/header');

get_template_part('loops/single-tag_services', 'loop');

get_template_part('templates/global/footer');

get_footer();